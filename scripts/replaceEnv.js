/**
 * @description
 *  临时的将线上数据库使用的账号密码导入 .env 文件
 * 
 */

const fs = require("fs");

{
    let params = process.argv.slice(2);
    if (params.length < 4) {
        console.error("参数不正确");
        process.exit(1);
    }
    console.log(params);

    
    let [ pathname, dataname, username, password ] = params;
    let str = fs.readFileSync(pathname).toString();

    str = str.replace(/<<<mysql_dataname>>>/, dataname);
    str = str.replace(/<<<mysql_username>>>/, username);
    str = str.replace(/<<<mysql_password>>>/, password);

    fs.writeFileSync(pathname, str);

    console.log("OK");
    process.exit(0);
}

