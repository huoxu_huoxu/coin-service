/**
 * @description
 *  统计
 * 
 */

require("dotenv").config();
process.env.realPath = "./";

const { QUERY } = require("../app/services/mysqlConnect");
const {
    runtimeCompatible: { getBinancePairsMap, calculateSettlementPrice2 }
} = require("../app/libs");

async function main (){
    var start_time = new Date("2018-09-01 00:00:00");
    var finish_time = new Date("2019-05-14 00:00:00");
    var b = start_time < finish_time;
    var d = 24 * 60 * 60 * 1000;

    var no = [];

    let ret, errObj;
    [ret, errObj] = await getBinancePairsMap();
    if (ret === undefined) {
        console.log("wallet record get binance pairs map error", errObj);
        return ;
    }
    let pairs = new Map();
    for(let item of ret){
        pairs.set(item.symbol, item.price);
    }

    while (b) {
        var start_stamptime = Math.ceil(start_time.getTime());
        var end_stamptime = start_stamptime + d;
        var end_time = new Date(end_stamptime);

        var orders = await QUERY(
            "select * from orders where created_at > ? and created_at < ?",
            [start_time, end_time]
        );

        let total = 0;
        if (orders.length > 0){
            for (let order of orders) {
                let orderProcess = await QUERY(
                    "select * from orders_process where order_id = ?",
                    [ order.id ]
                );
                if (orderProcess.length > 0) {
                    let tmpObj = {"asset": order.base_asset, "balance": Math.abs(orderProcess[0].trade)};
                    let tmpTotal = calculateSettlementPrice2([tmpObj], pairs);
                    total += tmpTotal;
                    if (tmpTotal === 0 && Math.abs(orderProcess[0].trade) > 0) {
                        no.push([order, orderProcess]);
                    }
                }
            }
        }
        let oDate = new Date(start_time.getTime() + 8 * 60 * 60);
        console.log(oDate.getFullYear() + "/" + (oDate.getMonth() + 1) + "/" + oDate.getDate() , ",",total);

        start_time = end_time;
        b = start_time < finish_time;
    }


    for (let v of no){
        console.log(v);
    }
    process.exit(0);
}


async function main2 (){
    var orders = await QUERY("select base_asset, quote_asset from orders");
    var m = new Map();

    for (let order of orders) {
        let name = order.base_asset + order.quote_asset;
        if (!m.has(name)) {
            m.set(name, "1");
        }
    }

    console.log(m.size);
    console.log(m);
    process.exit(0);
}

// main();
main2();


