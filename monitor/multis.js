/**
 *  @readme
 *      单机多核集群
 * 
 */

require("dotenv").config();

const cluster = require("cluster");
const numCPUs = require("os").cpus().length;

const {
    logConfigure: { runtime } 
} = require("../app/libs");

if (!module.parent) {

    runtime.info(`主进程 ${process.pid} 运行中`);
    runtime.info(`工作进程数: ${numCPUs}`);

    cluster.setupMaster({
        exec: "index.js"
    });

    for (let i=0; i<numCPUs; i++) {
        cluster.fork();
    }

    cluster.on("exit", (worker, code, signal) => {
        runtime.info(`工作进程 ${worker.process.pid} 已退出`);
        cluster.fork();
    });

}


