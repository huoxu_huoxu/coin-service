
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 *    兼容多算法模式   
 *    删除弃用字段 user_pov
 * 
 */

exports.up = function (db, callback) {
	db.removeColumn("orders", "user_pov", callback);
};

exports.down = function (db, callback) {
  return db.addColumn("orders", "user_pov", {
    type: "double",
    defaultValue: 0
  }, callback);
};

exports._meta = {
	"version": 1
};
