
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 * 	baskets - name 				字段唯一
 * 
 */

exports.up = function (db, callback) {
	db.addIndex("baskets", "unique_basket_name", "name", true, callback);
};

exports.down = function (db, callback) {
	return db.removeIndex("baskets", "unique_basket_name", callback);
};

exports._meta = {
	"version": 1
};
