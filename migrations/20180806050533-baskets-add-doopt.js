
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 * 	do_opt                    用户行为
 *    格式                    stop
 *    
 * 
 */

exports.up = function (db, callback) {
	db.addColumn("baskets", "do_opt", {
    type: "string",
    length: 255,
    defaultValue: ""
	}, callback);
};

exports.down = function (db, callback) {
	return db.removeColumn("baskets", "do_opt", callback);
};

exports._meta = {
	"version": 1
};
