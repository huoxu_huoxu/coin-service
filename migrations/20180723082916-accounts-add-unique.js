
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 * 	accounts - name 				字段唯一
 * 
 */

exports.up = function (db, callback) {
	db.addIndex("accounts", "unique_name", "name", true, callback);
};

exports.down = function (db, callback) {
	return db.removeIndex("accounts", "unique_name", callback);
};

exports._meta = {
	"version": 1
};
