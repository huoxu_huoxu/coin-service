
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 * 	代理账户表
 * 
 * @desc
 * 	display_name		代理账户名称, 备注
 * 	name				代理账号的用户名
 * 	
 * 
 */

exports.up = function (db, callback) {
	db.createTable("accounts", {
		id: {
			type: "int",
			primaryKey: true,
			autoIncrement: true,
			unsigned: true,
			notNull: true
		},
		display_name: {
			type: "string",
			length: 255,
			notNull: true
		},
		name: {
			type: "string",
			length: 255,
			notNull: true
		},
		updated_at: "datetime",
		created_at: "datetime"
	}, callback);
};

exports.down = function (db) {
	return db.dropTable("accounts");
};

exports._meta = {
	"version": 1
};
