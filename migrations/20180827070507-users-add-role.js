
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 * 	role                角色
 *    格式                0 普通用户、9 超级管理员用户		        
 * 
 */

exports.up = function (db, callback) {
	db.addColumn("users", "role", {
    type: "smallint",
    defaultValue: 0
	}, callback);
};

exports.down = function (db, callback) {
	return db.removeColumn("users", "role", callback);
};

exports._meta = {
	"version": 1
};
