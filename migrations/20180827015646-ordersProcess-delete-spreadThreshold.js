
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 *    兼容多算法模式   
 *    删除弃用字段 spread_threshold
 * 
 */

exports.up = function (db, callback) {
	db.removeColumn("orders_process", "spread_threshold", callback);
};

exports.down = function (db, callback) {
  return db.addColumn("orders_process", "spread_threshold", {
    type: "double",
    notNull: true	
  }, callback);
};

exports._meta = {
	"version": 1
};
