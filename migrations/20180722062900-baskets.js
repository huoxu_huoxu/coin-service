var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 * 	篮子表（需要进行交易的集合, 基于代理账户）
 * 
 * @desc
 * 	name		篮子编号/名称
 * 		UTC => BA + 年份 + 月份 + 日期 + 小时 + 分钟 + 秒  + 随机数字序列(6位) 
 * 	filename	用户上传的文件名称
 * 	account_id	代理账户 id
 * 	status		篮子的运行状态（new / running / final / ...）
 * 
 */

exports.up = function (db, callback) {
	db.createTable("baskets", {
		id: {
			type: "int",
			primaryKey: true,
			autoIncrement: true,
			unsigned: true,
			notNull: true
		},
		name: {
			type: "string",
			length: 255,
			notNull: true
		},
		filename: {
			type: "string",
			length: 255,
			notNull: true
		},
		account_id: {
			type: "int",
			unsigned: true,
			notNull: true
		},
		status: {
			type: "string",
			length: 16,
			defaultValue: "new"
		},
		updated_at: "datetime",
		created_at: "datetime"
	}, callback);
};

exports.down = function (db) {
	return db.dropTable("baskets");
};

exports._meta = {
	"version": 1
};
