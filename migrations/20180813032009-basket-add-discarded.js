
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 * 	discarded                 废弃的篮子
 *    格式                     (0,1)
 *    0 可用, 1 废弃
 *    
 * 
 */

exports.up = function (db, callback) {
	db.addColumn("baskets", "discarded", {
    type: "smallint",
    defaultValue: 0
	}, callback);
};

exports.down = function (db, callback) {
	return db.removeColumn("baskets", "discarded", callback);
};

exports._meta = {
	"version": 1
};
