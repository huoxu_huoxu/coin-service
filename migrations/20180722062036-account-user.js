
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 *  账户 - 代理账户 映射表
 * 
 * @desc
 * 	user_id			users表id
 * 	account_id		account表id
 * 
 */

exports.up = function (db, callback) {
	db.createTable("account_user", {
		id: {
			type: "int",
			primaryKey: true,
			autoIncrement: true,
			unsigned: true,
			notNull: true
		},
		user_id: {
			type: "int",
			unsigned: true,
			notNull: true
		},
		account_id: {
			type: "int",
			unsigned: true,
			notNull: true
		},
		updated_at: "datetime",
		created_at: "datetime"
	}, callback);
};

exports.down = function (db) {
	return db.dropTable("account_user");
};

exports._meta = {
	"version": 1
};
