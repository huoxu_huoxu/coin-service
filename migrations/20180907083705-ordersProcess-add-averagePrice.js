
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 * 	average_price         订单交易的平均价格
 * 
 */

exports.up = function (db, callback) {
	db.addColumn("orders_process", "average_price", {
    type: "double",
    defaultValue: 0
	}, callback);
};

exports.down = function (db, callback) {
	return db.removeColumn("orders_process", "average_price", callback);
};

exports._meta = {
	"version": 1
};
