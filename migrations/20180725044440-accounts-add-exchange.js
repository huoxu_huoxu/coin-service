
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 * 	exchange			交易所
 * 
 */

exports.up = function (db, callback) {
	db.addColumn("accounts", "exchange", {
		type: "string",
		length: 255,
		notNull: true,
		defaultValue: ""
	}, callback);
};

exports.down = function (db, callback) {
	return db.removeColumn("accounts", "exchange", callback);
};

exports._meta = {
	"version": 1
};
