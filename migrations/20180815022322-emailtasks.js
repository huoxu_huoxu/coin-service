
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 * 	邮件任务表
 * 
 * @desc
 * 	user_id			关联用户
 * 	step			间隔 分钟
 * 	accounts		包含的 account_id, 默认 "" 全部
 *  status			哪些状态的篮子
 * 	disabled		是否启用	true 不启用, false 启用
 * 
 */

exports.up = function (db, callback) {
	db.createTable("email_tasks", {
		id: {
			type: "int",
			primaryKey: true,
			autoIncrement: true,
			unsigned: true,
			notNull: true
		},
		user_id: {
			type: "int",
			unsigned: true,
			notNull: true
		},
		step: {
			type: "int",
			unsigned: true,
			notNull: true
		},
		accounts: {
			type: "string",
			defaultValue: ""
		},
		status: {
			type: "string",
			defaultValue: "running"
		},
		disabled: {
			type: "boolean",
			defaultValue: true
		},
		updated_at: "datetime",
		created_at: "datetime"
	}, callback);
};

exports.down = function (db) {
	return db.dropTable("users");
};

exports._meta = {
	"version": 1
};
