
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 * 	ended_at			  篮子运行结束时间
 * 
 */

exports.up = function (db, callback) {
	db.addColumn("baskets", "ended_at", {
		type: "datetime"
	}, callback);
};

exports.down = function (db, callback) {
	return db.removeColumn("baskets", "ended_at", callback);
};

exports._meta = {
	"version": 1
};
