
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 * 	实际交易表
 * 
 * @desc
 * 	basket_id					篮子表关联 id
 * 	base_asset					? 貌似是主键 币种A
 * 	quote_asset					币种B
 * 	direction					交易的动作（buy、sell）
 * 	algo_name					公式名称
 * 	algo_param					公式参数
 * 	start_time					开始时间
 * 	end_time					结束时间
 * 	qty							总量
 * 	user_pov					? running 中可改, 用户每天写 = 初始化为“” 
 * 	user_ spread_threshold		? 同上
 * 	do_opt						用户行为 ( pause, resume )
 * 
 */

exports.up = function (db, callback) {
	db.createTable("orders", {
		id: {
			type: "int",
			primaryKey: true,
			autoIncrement: true,
			unsigned: true,
			notNull: true
		},
		basket_id: {
			type: "int",
			unsigned: true,
			notNull: true
		},
		base_asset: {
			type: "string",
			length: 255,
			notNull: true
		},
		quote_asset: {
			type: "string",
			length: 255,
			notNull: true
		},
		direction: {
			type: "string",
			length: 100,
			notNull: true
		},
		algo_name: {
			type: "string",
			length: 255,
			notNull: true
		},
		algo_param: {
			type: "text",
			notNull: true
		},
		start_time: "datetime",
		end_time: "datetime",
		qty: {
			type: "double",
			notNull: true
		},
		user_pov: {
			type: "double",
			defaultValue: 0
		},
		user_spread_threshold: {
			type: "double",
			defaultValue: 0
		},
		do_opt: {
			type: "string",
			length: 255,
			defaultValue: ""
		},
		note: "text",
		updated_at: "datetime",
		created_at: "datetime"
	}, callback);
};

exports.down = function (db) {
	return db.dropTable("orders");
};

exports._meta = {
	"version": 1
};
