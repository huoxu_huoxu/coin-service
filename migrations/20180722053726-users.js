
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 * 	主账户表 / 子账户表
 * 
 * @desc
 * 	parent_id		上级账号 users-id
 * 	nickname		用户昵称
 * 	email			用户邮箱 / 登录账号
 * 	password		密码 - 加盐密文
 * 	salt			盐值
 * 	remember_tokrn	忘记密码使用的token, 修改密码时使用 可以拆 redis 处理
 * 
 * 
 * 	权限:
 * 		管理员权限			新增字段 role tinyint, 0 - 9, default 0， 9 超管, 可以管理所有的用户, 从用户下拉中选择管理用户
 * 		用户权限			目前有 parent_id int, 负责用户之间的继承管理关系
 * 						   新增字段 modules text-json string{}, 可以由上级用户管理下级用户所能浏览的模块及改写的模块 (有待之后在拓展, 暂时的构想)	
 * 
 */

exports.up = function (db, callback) {
	db.createTable("users", {
		id: {
			type: "int",
			primaryKey: true,
			autoIncrement: true,
			unsigned: true,
			notNull: true
		},
		parent_id: {
			type: "int",
			unsigned: true,
			defaultValue: 0
		},
		nickname: {
			type: "string",
			length: 255,
			notNull: true
		},
		email: {
			type: "string",
			length: 255,
			notNull: true
		},
		password: {
			type: "string",
			length: 255,
			notNull: true
		},
		salt: {
			type: "string",
			length: 10,
			notNull: true
		},
		remember_token: {
			type: "string",
			length: 100
		},
		updated_at: "datetime",
		created_at: "datetime"
	}, callback);
};

exports.down = function (db) {
	return db.dropTable("users");
};

exports._meta = {
	"version": 1
};
