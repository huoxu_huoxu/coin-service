
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 * 	name			uuid
 * 
 */

exports.up = function (db, callback) {
	db.addColumn("orders", "name", {
		type: "string",
		length: 255,
		notNull: true
	}, callback);
};

exports.down = function (db, callback) {
	return db.removeColumn("orders", "name", callback);
};

exports._meta = {
	"version": 1
};
