
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 *    兼容多算法模式   
 *    删除弃用字段 pov
 * 
 */

exports.up = function (db, callback) {
	db.removeColumn("orders_process", "pov", callback);
};

exports.down = function (db, callback) {
  return db.addColumn("orders_process", "pov", {
    type: "double",
    notNull: true	
  }, callback);
};

exports._meta = {
	"version": 1
};
