
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 * 	代表 每笔交易的进程
 * 
 * @desc
 * 	order_id			orders表关联 id
 * 	pov					?
 * 	spread_threshold	?
 * 	trade				已完成
 * 	notrade				未完成
 * 	pct					百分比
 * 	status				这笔交易单的状态（finished / ...）
 * 	error_msg			错误信息
 * 
 */

exports.up = function (db, callback) {
	db.createTable("orders_process", {
		id: {
			type: "int",
			primaryKey: true,
			autoIncrement: true,
			unsigned: true,
			notNull: true
		},
		order_id: {
			type: "int",
			unsigned: true,
			notNull: true
		},
		pov: {
			type: "double",
			notNull: true		
		},
		spread_threshold: {
			type: "double",
			notNull: true		
		},
		trade: {
			type: "double",
			notNull: true	
		},
		notrade: {
			type: "double",
			notNull: true	
		},
		pct: {
			type: "double",
			notNull: true	
		},
		status: {
			type: "string",
			length: 100,
			notNull: true
		},
		error_msg: {
			type: "string",
			length: 255,
			notNull: true
		},
		updated_at: "datetime",
		created_at: "datetime"
	}, callback);
};

exports.down = function (db) {
	return db.dropTable("orders_process");
};

exports._meta = {
	"version": 1
};
