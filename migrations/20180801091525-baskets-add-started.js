
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 * 	started_at			  篮子开始运行时间
 * 
 */

exports.up = function (db, callback) {
	db.addColumn("baskets", "started_at", {
		type: "datetime"
	}, callback);
};

exports.down = function (db, callback) {
	return db.removeColumn("baskets", "started_at", callback);
};

exports._meta = {
	"version": 1
};
