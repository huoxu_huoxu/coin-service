
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 * 	user_algo_param		        实际的算法参数 - 用户设置的算法参数, call灰灰接口获得
 * 
 */

exports.up = function (db, callback) {
	db.addColumn("orders", "user_algo_param", {
		type: "text"
	}, callback);
};

exports.down = function (db, callback) {
	return db.removeColumn("orders", "user_algo_param", callback);
};

exports._meta = {
	"version": 1
};
