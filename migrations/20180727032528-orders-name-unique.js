
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 * 	orders - name 				字段唯一
 * 
 */

exports.up = function (db, callback) {
	db.addIndex("orders", "unique_order_name", "name", true, callback);
};

exports.down = function (db, callback) {
	return db.removeIndex("orders", "unique_order_name", callback);
};

exports._meta = {
	"version": 1
};
