
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 * 	balance                   账户各币种余额
 *    格式                    [ { asset 币种, balance 余额 }, ... ]
 *    
 * 
 */

exports.up = function (db, callback) {
	db.addColumn("accounts", "balance", {
		type: "text"
	}, callback);
};

exports.down = function (db, callback) {
	return db.removeColumn("accounts", "balance", callback);
};

exports._meta = {
	"version": 1
};
