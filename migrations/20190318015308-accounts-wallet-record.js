
var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function (options, seedLink) {
    dbm = options.dbmigrate;
    type = dbm.dataType;
    seed = seedLink;
    console.log(dbm, type, seed);
};

/**
 * @description
 *  仓位及结算价记录, 08:00 +0800 
 *  
 * @desc
 *  account_id          账户关联表 id 
 *  seetlement_price    结算价(usdt计价)
 *  pos                 持仓
 * 
 */


exports.up = function (db, callback) {
    db.createTable("wallet_recorded", {
        id: {
            type: "int",
            primaryKey: true,
            autoIncrement: true,
            unsigned: true,
            notNull: true
        },
        account_id: {
            type: "int",
            unsigned: true,
            notNull: true
        },
        settlement_price: {
            type: "double",
            unsigned: true,
            notNull: true
        },
        pos: "text",
        updated_at: "datetime",
        created_at: "datetime"
    }, callback);
};

exports.down = function (db, callback) {
    return db.dropTable("wallet_recorded");
};

exports._meta = {
    "version": 1
};
