
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 *    修改关联逻辑   
 *    删除弃用字段 accounts
 * 
 */

exports.up = function (db, callback) {
	db.removeColumn("email_tasks", "accounts", callback);
};

exports.down = function (db, callback) {
  return db.addColumn("email_tasks", "accounts", {
			type: "string",
			defaultValue: ""
  }, callback);
};

exports._meta = {
	"version": 1
};
