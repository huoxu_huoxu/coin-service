
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 * 	mailbox               	关联邮箱
 *    格式                  xxx,xxx,xx
 *    
 * 
 */

exports.up = function (db, callback) {
	db.addColumn("accounts", "mailbox", {
		type: "text"
	}, callback);
};

exports.down = function (db, callback) {
	return db.removeColumn("accounts", "mailbox", callback);
};

exports._meta = {
	"version": 1
};
