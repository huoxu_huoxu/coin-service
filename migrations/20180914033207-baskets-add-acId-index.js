
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 * 	baskets         account_id 改为 index key
 * 
 */

exports.up = function (db, callback) {
  db.addIndex("baskets", "baskets_index_acId", "account_id", callback);
  db.addIndex("baskets", "baskets_index_status", "status", callback);
};

exports.down = function (db, callback) {
  db.removeIndex("baskets", "baskets_index_status", callback);
	return db.removeIndex("baskets", "baskets_index_acId", callback);
};

exports._meta = {
	"version": 1
};
