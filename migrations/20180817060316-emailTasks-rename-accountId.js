
var dbm;
var type;
var seed;

exports.setup = function (options, seedLink) {
	dbm = options.dbmigrate;
	type = dbm.dataType;
	seed = seedLink;
	console.log(dbm, type, seed);
};

/**
 * @description
 *    修改关联逻辑   
 *    account_id 关联 邮件任务
 * 
 */

exports.up = function (db, callback) {
	db.renameColumn("email_tasks", "user_id", "account_id", callback);
};

exports.down = function (db, callback) {
  return db.renameColumn("email_tasks", "account_id", "user_id", callback);
};

exports._meta = {
	"version": 1
};
