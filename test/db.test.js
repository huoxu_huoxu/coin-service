/**
 * @description
 *  测试数据库内存溢出
 * 
 */

require("dotenv").config();

const mysql = require('mysql');
const mysql_config = {
    connectionLimit: 10,
    host: process.env.DB_HOST,
    post: process.env.DB_PORT,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    multipleStatements: true
};
let pool = mysql.createPool(mysql_config);

// 通用语句
const QUERY = function (...argv){
    return new Promise(function(resolve, reject){
        pool.getConnection((err, conn) => {
            if(err){
                err.status = 12001;
                throw err;
            }
            conn.query(...argv, function(err, rows, fields){
                conn.release();
                if(err){
                    err.status = 12002;
                    reject(err);
                    return ;
                }
                resolve(rows);
            });
        });
    });
};


const aSql = [
    "select * from users",
    "select * from accounts",
    "select * from account_user",
    "select * from baskets",
    "select * from email_tasks",
    "select * from orders",
    "select * from orders_process"
];

{
    let i = aSql.length;
    let counts = 0;
    setInterval(async function (){
        counts++;
        let index = Math.floor(Math.random() * i);
        await QUERY(aSql[index]);
        console.log("OK",  aSql[index], counts);
    }, 500);
}




