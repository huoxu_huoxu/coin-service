/**
 * @description
 *  测试自签名TLS证书 可用性
 * 
 */

const https = require("https");

const data = {
    "email": "530607199@qq71111.com",
    "nickname": "huoxu_huoxu",
    "password": "xxxx"
};
const s = JSON.stringify(data);

const options = {
    hostname: 'test.quantdog.com',
    port: 443,
    path: '/service/api/user/register',
    method: 'POST',
    // key: fs.readFileSync('./tmp/diy/ssl.key'),
    // cert: fs.readFileSync('./tmp/diy/ssl.crt'),
    headers: {
        'Content-Type': "application/json",
        'Content-Length': s.length
    },
    rejectUnauthorized: false
};

const req = https.request(options, (res) => {
    console.log(`状态码: ${res.statusCode}`);
    console.log(`响应头: ${JSON.stringify(res.headers)}`);
    res.setEncoding('utf8');
    res.on('data', (chunk) => {
        console.log(`响应主体: ${chunk}`);
    });
    res.on('end', () => {
        console.log('响应中已无数据。');
    });
});

req.on('error', (e) => {
    console.error(`请求遇到问题: ${e.message}`);
});

// 写入数据到请求主体
req.write(s);
req.end();


