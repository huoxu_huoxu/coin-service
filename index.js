/**
 * @description
 *  完成启动前的自检
 *  启动业务服务器进程
 * 
 */

require("dotenv").config();

const http = require("http");
const { selfExamination } = require("./app/tasks");


{

    if (!module.parent) {

        // 标记项目根目录
        process.env.realPath = __dirname;

        (async () => {

            // 自检
            await selfExamination();

            // 启动服务
            const app = require("./app/app");
            const email_tasks = require("./app/tasks/email_tasks");
            const wallet_record = require("./app/tasks/wallet_record_initerval")

            const PORT = process.env.PORT;

            const {
                logConfigure: { runtime, loggerErr }
            } = require("./app/libs");

            const server = http.createServer(app);
            server.listen(PORT);

            server.on("listening", () => {
                runtime.info(`工作进程 pid: ${process.pid} 已启动, 监听端口号 ${PORT}...`);
            });

            server.on("error", (error) => {
                if (error.syscall !== "listen") throw error;

                switch (error.code) {
                    case 'EACCES':
                        loggerErr.error(PORT + ' requires elevated privileges');
                        process.exit(1);
                        break;
                    case 'EADDRINUSE':
                        loggerErr.error(PORT + ' is already in use');
                        process.exit(1);
                        break;
                    default:
                        throw error;
                }
                
            });

            process.on("uncaughtException", (msg) => {
                loggerErr.warn(`uncaughtException: ${msg}`);
            });

            process.on("unhandledRejection", (rejected) => {
                loggerErr.warn(`unhandledRejection: ${rejected}`);
            });
    
    
            // 启动定时
            email_tasks();
            wallet_record();

        })();
        
    }

}
