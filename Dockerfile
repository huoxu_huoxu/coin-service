FROM node:10.9.0

EXPOSE 3000

ENV AP /main

WORKDIR $AP

ADD ./package.json $AP

RUN npm install cnpm -g --registry=https://registry.npm.taobao.org
RUN cnpm install --production --registry=https://registry.npm.taobao.org

ADD ./.env.docker $AP/.env

ADD . $AP

CMD [ "node", "index.js" ]


