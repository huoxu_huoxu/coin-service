/**
 * @description
 *  读取文件内容
 * 
 */
const path = require("path");
const fs = require("fs");


// 读取csv
const csvReadFile = (filename) => {

    let content = fs.readFileSync(filename);
    let arr = content.toString().split("\r\n");
    let aList = [];
    for(let s of arr){
        let a = s.split(",");
        if(a && a.length > 1) aList.push(a);
    }

    return aList;

};


// 解析文件内容, 生成可用格式
module.exports.parseFile = (filename) => {

    let extname = path.extname(filename);
    let realPath = path.resolve(process.env.realPath, filename);

    if (extname === ".csv"){
        return csvReadFile(realPath);
    }

    return [];
    
};

