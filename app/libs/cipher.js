/**
 * @description
 *  对称加密
 * 
 * @function
 *  jwtSign         加密
 *  jwtVerify       解密
 *  createMd5       md5散列
 * 
 * 
 */

const crypto = require("crypto");
const jwt = require('jsonwebtoken');

exports.jwtSign = (expTimes, data, key = "qazwsxz") => {
    let verify = jwt.sign({
        exp: Math.floor(Date.now() / 1000) + expTimes,
        data: data
    }, key);

    return verify;
};

exports.jwtVerify = (token, key = "qazwsxz") => {
    try{    
        return jwt.verify(token, key);
    }catch(err){
        return false;
    }
};

module.exports.createMd5 = (s = ''+Date.now()) => {
    let hash = crypto.createHash("md5");
    hash.update(s);
    return hash.digest("hex");
};


