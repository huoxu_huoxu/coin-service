/**
 * @description
 *  自定义类
 * 
 *  
 * @class
 *  CreateHttpError     处理 http status 非200情况
 * 
 */

const {
    loggerErr
} = require("./logConfigure");

class CreateHttpError extends Error {

    constructor (info, msg = "", status = 500){
        super(msg);
        loggerErr.fatal(msg);
        this.errInfo = info;
        this.status = status;
    }

}

module.exports = {
    CreateHttpError
};

