/**
 * @description
 *  按格式生成需要的邮件内容
 * 
 * @function
 *  formatEmailAddress          生成可发送地址格式
 *  formatBasketProgress        格式化生成篮子进度报告
 * 
 */

/**
 * 
 * @param {*} users             主账号数组, [ { nickname, email }, { ... } ]
 * @param {*} mailbox           关联账号,   "xxx,xxx,xxx"
 */
const formatEmailAddress = (users, mailbox) => {
    let emails = users.map(v => ({ name: v.nickname, address: v.email }));
    if (mailbox){
        let mailboxes = mailbox.split(",");
        for (let item of mailboxes){
            emails.push({ name: "", address: item });
        }
    }
    return emails;
};
const formatBasketProgress = (obj_baskets) => {
    let html = "";
    for (let v of Object.values(obj_baskets)){

        html += `
            <h4>篮子: ${v.name}</h4>
            <table border="1px" width="400px" cellpadding="4px">
                <tr>
                    <th>币1/币2</th>
                    <th>buy/sell</th>
                    <th width="70px">进度</th>
                    <th>status</th>
                    <th>总量</th>
                    <th>已完成</th>
                    <th>余额</th>
            </tr>
        `;

        for (let order of v){
            html += `
                <tr align="center">
                    <td>${order.base_asset}/${order.quote_asset}</td>
                    <td>${order.direction}</td>
                    <td>${order.pct || 0}%</td>
                    <td>${order.status || "new"}</td>
                    <td>${order.qty}</td>
                    <td>${order.trade || 0}</td>
                    <td>${order.balance ? order.balance : 0}</td>
                </tr>
            `;
        }

        // 不是所有算法都有的字段, 暂时邮件先不发送
        // <td>${order.pov || 0}</td>
        // <td>${order.spread_threshold || 0}</td>

        html += "</table>";
    }
    return html;
};





module.exports = {
    formatEmailAddress,
    formatBasketProgress
};

