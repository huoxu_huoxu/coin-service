/**
 * @description
 *  日志体系
 * 
 * @function
 *  logger              接口请求/返回 日志记录
 *  loggerErr           错误日志记录
 *  runtime             运行时日志记录
 *  loggerEmailTask     邮件发送日志
 *  loop_write          轮询写日志
 * 
 * 
 */

const log4js = require("log4js");

log4js.configure({
    appenders: {
        everything: { type: "stdout", pattern: '.yyyy-MM-dd-hh' },
        req: { type: "dateFile", filename: `${process.env.LOGGER_PATH}/logs/`, pattern: 'req-yyyy-MM-dd.log', alwaysIncludePattern: true, compress: true },
        err: { type: "dateFile", filename: `${process.env.LOGGER_PATH}/errlogs/`, pattern: 'err-yyyy-MM-dd.log', alwaysIncludePattern: true, compress: true },
        runtime: { type: "dateFile", filename: `${process.env.LOGGER_PATH}/runtime/`, pattern: 'runtime-yyyy-MM-dd.log', alwaysIncludePattern: true, compress: true },
        email_task: { type: "dateFile", filename: `${process.env.LOGGER_PATH}/tasks/`, pattern: 'emailTask-yyyy-MM-dd.log', alwaysIncludePattern: true, compress: true },
        wallet_record: { type: "dateFile", filename: `${process.env.LOGGER_PATH}/wallet/`, pattern: 'walletRecord-yyyy-MM-dd.log', alwaysIncludePattern: true, compress: true },
        loop_write: { type: "dateFile", filename: `${process.env.LOGGER_PATH}/loops/`, pattern: 'loopWrite-yyyy-MM-dd.log', alwaysIncludePattern: true, compress: true }
    },
    categories: {
        // 接口数据日志记录
        default: { appenders: [ 'everything', 'req' ], level: 'info' },
        // 错误记录
        err: { appenders: [ 'everything', 'err' ], level: 'warn' },

        // 运行时打印信息
        runtime: { appenders: [ 'everything', 'runtime' ], level: 'debug' },

        // 定时任务日志
        email_task: { appenders: [ 'everything', 'email_task' ], level: process.env.DEBUG ? "debug" : "info" },
        
        // 定时记录账户仓位
        wallet_record: {appenders: [ 'everything', 'wallet_record' ], level: process.env.DEBUG ? "debug" : "info"  },

        // 轮询写操作记录
        loop_write: { appenders: [ 'everything', 'loop_write' ], level: "info" }
    }
});

exports.logger = log4js.getLogger();
exports.loggerErr = log4js.getLogger("err");
exports.runtime = log4js.getLogger("runtime");
exports.loggerEmailTask = log4js.getLogger("email_task");
exports.loggerLoopWrite = log4js.getLogger("loop_write");
exports.loggerWalletRecord = log4js.getLogger("wallet_record");
