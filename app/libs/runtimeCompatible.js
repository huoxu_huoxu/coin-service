/**
 * @description
 *  运行时兼容
 * 
 * 
 * @desc
 *  getBinancePairsMap          获取binance的交易对价格
 *  
 * 
 */

const https = require("https");
const path = require("path");
const fs = require("fs");

const getBinancePairsMap = async () => {
    let ret, errObj;
    if (process.env.DEBUG) {
        let tmpStr = fs.readFileSync(path.resolve(process.env.realPath, "./local/ratio.json"), { "encoding": "utf-8" });
        ret = JSON.parse(tmpStr);
    } else {
        let tmpStr = await new Promise((resolve, reject) => {
            let timer = setTimeout(() => {
                resolve("");
            }, 5000);
            https.get("https://api.binance.com/api/v3/ticker/price", (res) => { 
                let chunks = [];
                res.on("data", (chunk) => {
                    chunks.push(chunk);
                });
                res.on("end", () => {
                    clearTimeout(timer);
                    timer = null;
                    resolve(chunks.join(""));
                });
            }).on("error", (err) => {
                clearTimeout(timer);
                timer = null;
                errObj = err;
                resolve("");
            });
        });
        if (tmpStr !== ""){
            ret = JSON.parse(tmpStr);
        }
    }
    return [ ret, errObj ];
};


const calculateSettlementPrice = (balances, pairs) => {
    let btc = 0, usdt = 0, total = 0;

    /**
     * @生成数据结构
     *  {
     *      asset: 币,
     *      balance: 仓位,
     *      btc: 等值BTC,
     *      usdt: 等值USDT,
     *  }   
     * 
     */

    var btcUsdtRatio = pairs.get("BTCUSDT")
    for (let v of balances){
        v["btc"] = "N/A"
        v["usdt"] = "N/A"

        switch (v.asset) {
            case "BTC":
                v["btc"] = v.balance;
                v["usdt"] = v.balance * btcUsdtRatio;
                break
            case "USDT":
                v["usdt"] = v.balance;
                v["btc"] = v.balance / btcUsdtRatio;
                break
            case "USDC":
                v["usdt"] =  v.balance * pairs.get("USDCUSDT")
                v["btc"] = v["usdt"] / btcUsdtRatio;
                break
            default:
                let key = v.asset+"USDT";
                if (pairs.has(key)) {
                    let price = pairs.get(key);
                    v["usdt"] = v.balance * price;
                    v["btc"] =  v["usdt"]  / btcUsdtRatio;
                    break
                }

                key = v.asset+"BTC";
                if (pairs.has(key)) {
                    let price = pairs.get(key);
                    v["btc"] = v.balance * price;
                    v["usdt"] = v["btc"] * btcUsdtRatio;
                    break
                }
        }
    }

    return balances;
};


const calculateSettlementPrice2 = (balances, pairs) => {
    let btc = 0, usdt = 0, total = 0;
    let btcUsdtRatio = pairs.get("BTCUSDT")
    for (let v of balances){
        if (v.asset === "BTC") {
            btc += v.balance;
        } else if (v.asset === "USDT") {
            usdt += v.balance;
        } else if (v.asset === "UST"){
            usdt += v.balance;
        } else if (v.asset === "USDC"){
            usdt += v.balance * pairs.get("USDCUSDT");
        } else if (v.asset === "XBTUSD") {
            // bitmex 成交量继承
            usdt += v.balance;
        } else if (v.asset === "ETHUSD"){
            let key = "ETHUSDT";
            if (pairs.has(key)){
                let price = pairs.get(key);
                btc += price * 0.000001 * v.balance;
            } else {
                // console.log("不存在此key", v.asset);
            }
        } else {
            let key = v.asset + "USDT";
            if (pairs.has(key)){
                let price = pairs.get(key);
                let tmpUsdtBalance = v.balance * price;
                btc += tmpUsdtBalance / btcUsdtRatio
                continue
            }

            key = v.asset + "BTC";
            if (pairs.has(key)){
                let price = pairs.get(key);
                btc += v.balance * price;
            }
        }
    }
    total = usdt + (btcUsdtRatio * btc);
    return total;
};


exports.getBinancePairsMap = getBinancePairsMap;
exports.calculateSettlementPrice = calculateSettlementPrice;
exports.calculateSettlementPrice2 = calculateSettlementPrice2;

