/**
 * @description
 *  基础工具
 * 
 * @function
 *  test                        检测数据类型
 *  getUtcDate                  获取 UTC 年、月、日、小时、分钟、秒 
 *  randomNums                  生成随机序列长度的整数字符串
 * 
 * 
 */


const test = (() => {

    const type_arr = [
        "Function",
        "Object",
        "Array",
        "Symbol",
        "Set",
        "Map",
        "WeakSet",
        "WeakMap",
        "String",
        "Number",
        "Boolean"
    ];

    const tmp = {};

    for ( const type_name of type_arr){
        tmp[`is${type_name}`] = (v) => {
            return Object.prototype.toString.call(v) === `[object ${type_name}]`;
        };
    }

    // isNumber 方法 - 追加条件
    const tmp_number = tmp["isNumber"];
    tmp["isNumber"] = (v) => tmp_number(v) && !Number.isNaN(v);

    return tmp;

})();


const getUtcDate = () => {
    let a = new Date();
    let y = a.getUTCFullYear(),
        m = a.getUTCMonth(),
        d = a.getUTCDate(),
        h = a.getUTCHours(),
        mm = a.getUTCMinutes(),
        s = a.getUTCSeconds();

        m += 1;
        m = m < 10 ? "0" + m : m;
        d = d < 10 ? "0" + d : d;
        h = h < 10 ? "0" + h : h;
        mm = mm < 10 ? "0" + mm : mm;
        s = s < 10 ? "0" + s : s;

    return [ y, m, d, h, mm, s ];
};

const getCurrentDate = (timestamp) => {
    let a;
    if (timestamp) a = new Date(timestamp);
    else a = new Date();

    let y = a.getFullYear(),
        m = a.getMonth(),
        d = a.getDate(),
        h = a.getHours(),
        mm = a.getMinutes(),
        s = a.getSeconds();

        m += 1;
        m = m < 10 ? "0" + m : m;
        d = d < 10 ? "0" + d : d;
        h = h < 10 ? "0" + h : h;
        mm = mm < 10 ? "0" + mm : mm;
        s = s < 10 ? "0" + s : s;

    return [ y, m, d, h, mm, s ];
};

const randomNums = (len = 6) => {
    let t = 10 ** len;
    let i = (Math.round(Math.random() * t)).toString();
    let a = i.split("");
    a.unshift(Array(len - a.length).fill("0").join(""));
    return a.join("");
};



module.exports = {
    test,
    getUtcDate,
    getCurrentDate,
    randomNums
};
