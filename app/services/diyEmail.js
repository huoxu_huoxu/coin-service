/**
 * @description
 *  邮件服务
 * 
 * 
 */

const nodemailer = require("nodemailer");
const smtpTransport = require("nodemailer-smtp-transport");

const {
    logConfigure: {
        loggerEmailTask
    }
} = require("../libs");

// 邮件格式
const reg_email = /\w+@.+\..{1,5}/;

// 密送地址
let always = [];
{
    if (process.env.DEBUG) {
        always = [
            {
                name: "测试地址",
                address: "zhen.hu@ib-asset.com"
            }
        ];
    } else if  (process.env.NO_SEND_EMAIL) {
        always = [];
    } else {
        always = [
            {
                name: "QuantDog",
                address: "trading@ib-asset.com"
            }
        ];
    }
}

// 建立邮件发送端
const st = nodemailer.createTransport(smtpTransport({
	host: "smtp.exmail.qq.com",
    port: 465,
	auth: {
		user: "postman@quantdog.com",
		pass: "HuZhen1607"
	}
}));

/**
 * @description
 *  通用发送
 * 
 * @param {*} params                邮件信息 { subject, [ html / text ] }
 * @param {*} to_email              发送地址 [ { name, address } ] 
 * @param {*} account_id            所属方
 */
module.exports.currencySendMail = (params, to_email, account_id) => {

    let mail_params;

    // 检查发送地址是否合法, 过滤非法
    to_email = to_email.filter(v => {
        let b = reg_email.test(v.address);
        if (!b){
            loggerEmailTask.warn("account_id 关联邮件, 不符合邮件格式", account_id, v.address);
        }
        return b;
    });
    
    // 涉及 - 只发送公司系统邮箱不发送给用户的情况
    // if (!to_email.length) return loggerEmailTask.warn("account_id 不存在可以发送邮件的有效目标地址", account_id);

    // 生成邮件基础信息
    // 暂时全部密送, 之后升级版再说
    mail_params = {
        from: "QuantDog<postman@quantdog.com>",
        // to: ,
        subject: "",
        bcc: [ ...to_email, ...always ]
    };
    mail_params = Object.assign({}, mail_params, params);

    st.sendMail(mail_params, (err, res) => {
        if (err) return loggerEmailTask.error("发送邮件, 发生了错误", err.toString());
        loggerEmailTask.debug(res);
        loggerEmailTask.info("email task succ", to_email.map(v => v.address).join(","));
    });
}; 

