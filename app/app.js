// app - service

const Express = require("express");
const multer  = require('multer');

const app = Express();
const router = Express.Router();
const web = Express.Router();
const superAdmin = Express.Router();

// 接口
const {
    orders: { 
        updateOrders, 
        getOrders,
        webGetOrders,
        webUpdateOrder,
        webExportOrders
    },
    baskets: { 
        uploadBasket,
        getBaskers,
        downloadBasketFile,
        inquiryBasket,
        webUpdateBasket,
        updateBasket,
        webDeleteBasket
    },
    accounts: {
        getAccountRelationBaskets,
        getOrdersRelationAB,
        addAccount,
        getAccounts,
        setAccountBalance,
        webExportAccountBalance,
        webExportSettlementPrice
    }
} = require("./controllers");


// 中间件
const {
    verifyCipher: { 
        verifyToken,
        verifyTokenUploadFile
    },
    uploadFile: {
        storage
    },
    relaTables: {
        relaUserActionBasket,
        verifySuperRole,
        relaUserActionAccount
    },
    secure: {
        access_restrictions
    },  
    mw
} = require("./middlewares");

// other
const {
    diyClass: { CreateHttpError }
} = require("./libs");


// init upload path
const upload = multer({ storage: storage });


// middlewares 
{
    app.use(mw);
}


// web page
{

    const ver = Express.Router();
    ver.use(access_restrictions, verifyToken);

    // 获取accounts列表
    web.get("/api/data/getAccounts", ver, getAccounts);

    // 获取baskets列表
    web.get("/api/data/getBaskers/:account_id", ver, getBaskers);

    // 获取orders列表
    web.get("/api/data/getOrders/:account_id/:basket_id", ver, webGetOrders);

    // 下载仓位快照
    web.get("/api/download/accountBalance/:account_id", ver, webExportAccountBalance);

    // 下载篮子原始文件
    web.get("/api/download/:account_id/:basket_id", ver, relaUserActionBasket, downloadBasketFile);

    // 询问是否可以下载篮子
    web.post("/api/download/:account_id/:basket_id", ver, relaUserActionBasket, inquiryBasket);

    
    // 添加account
    web.post("/api/user/addAccount", ver, addAccount);
    
    // 篮子 - 更新字段
    web.put("/api/basket/:account_id/:basket_id", ver, relaUserActionBasket, webUpdateBasket);

    // 篮子 - 删除篮子
    web.delete("/api/basket/:account_id/:basket_id", ver, relaUserActionBasket, webDeleteBasket);

    // 订单 - 更新字段
    web.put("/api/order/:account_id/:basket_id/:order_id", ver, relaUserActionBasket, webUpdateOrder);


    // 上传文件
    web.post("/api/baskets/upload", access_restrictions, verifyTokenUploadFile, upload.single("backetsCsv"), uploadBasket);


    app.use(web);
}


// super admin
{

    const verSuper = Express.Router();
    verSuper.use(verifyToken, verifySuperRole);

    // 超管 - 导出一段时间内运行的所有订单
    superAdmin.get("/api/data/orders", verSuper, relaUserActionAccount, webExportOrders);

    // 超管 - 导出一段时间内的账户结算价
    superAdmin.get("/api/data/walletRecorded", verSuper, relaUserActionAccount, webExportSettlementPrice);


    app.use(superAdmin);
}


// router
{
    // 获取新的篮子的名称
    router.get("/api/baskets/new/:account_name", getAccountRelationBaskets);

    // 获取篮子内的订单详情
    router.get("/api/baskets/info/:account_name/:basket_name", getOrdersRelationAB);

    // 通过篮子名称获取订单详情
    router.get("/api/baskets/orders/:basket_name", getOrders);

    // 设置账户余额
    router.put("/api/setAccountBalance/:account_name", setAccountBalance);

    // 篮子 - 更新字段
    router.put("/api/updateBasket/:basket_name", updateBasket);


    // 更新orders进行状态
    router.put("/api/orders/updateProcess/:basket_name", updateOrders);


    app.use(router);
}


// 404
app.use((req, res, next) => {
    let error = new Error("not found");
    error.status = 404;
    next(error);
});

// 500
app.use((err, req, res, next) => {

    let errcode = err.status || 500;
    let msg = err.toString();

    // 变更响应方式
    if (err instanceof CreateHttpError){
        return res.status(errcode).json(err.errInfo);
    }

    return res.json({
        errcode,
        msg
    });

});


module.exports = app;
