/**
 * @description
 *  验证相关
 * 
 * 
 * @function
 * 
 *  verifyHash              验证非 get 类的请求
 *  verifyGetHash           验证 get 类的请求
 * 
 *  verifyToken             验证用户生命周期
 *  verifyTokenUploadFile   文件上传时 验证用户生命周期
 * 
 */

const {
    cipher: { createMd5, jwtVerify },
    tools: { getUtcDate },
} = require("../libs");

const { o1002, o1100, o1102 } = require("../result");

const hash_key = "aixi2018";


module.exports.verifyGetHash = (req, res, next) => {

    let { cipher } = req.body;
    let [ y, m, d, h, mm ] = getUtcDate();
    let m1 = (+mm) + 1;

    let s1 = createMd5( y + m + d + h + mm + hash_key);
    let s2 = createMd5( y + m + d + h + m1 + hash_key);

    if (cipher === s1 || cipher === s2) return next();

    return res.json(o1002);

};


module.exports.verifyHash = (req, res, next) => {

    try {

        let { cipher } = req.body;
        delete req.body['cipher'];
        let s = createMd5(req.body.toString());
        
        if (cipher === s) return next();

        return res.json(o1002);

    } catch (err){
        next(err);
    }

};



module.exports.verifyToken = (req, res, next) => {

    try {

        let { token } = req.body;

        if (!token) return res.json(o1102);

        let resul = jwtVerify(token);
        if (!resul) return res.json(o1100);

        // 记录用户信息
        req.ctx.__userInfo = resul;

        next();

    } catch (err){
        next(err);
    }

};


module.exports.verifyTokenUploadFile = (req, res, next) => {

    try {

        let { token } = req.body;

        let resul = jwtVerify(token);
        if (!resul) throw Error();
        
        req.ctx.__userInfo = resul;
        
        next();

    } catch (err){

        res.set( "Content-Type", "application/json; charset=utf-8" );
        res.status(403).json(o1100);

    }

};
