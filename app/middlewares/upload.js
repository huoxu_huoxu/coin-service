/**
 * @description
 *  上传中间件
 * 
 * 
 * @function
 *  storage                 上传篮子文件方案
 *  
 */

const multer  = require('multer');
const {
    tools: {
        getUtcDate, 
        randomNums
    }
} = require("../libs");
const { UPLOAD_PATH } = process.env;

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, UPLOAD_PATH);
    },
    filename: function (req, file, cb) {
        let ext = randomNums();
        let basket_name = "BA" + getUtcDate().join("") + ext;
        file.basket_name = basket_name;
        cb(null, basket_name + ".csv");
    }
});


module.exports = {
    storage
};
