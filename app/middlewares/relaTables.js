/**
 * @description
 *  多数据表之间的关联验证
 * 
 * @function
 *  relaUserAction          验证进行basket操作的用户是否非法
 * 
 *  verifySuperRole         验证账户是否是超管角色
 *  relaUserActionAccount   验证超管角色发起的请求 用户和账户是否关联
 * 
 */

const { QUERY } = require("../services/mysqlConnect");
const {
    models: { Users, AccountUser }
} = require("../models");
const { o4200, o5400 } = require("../result");


module.exports.relaUserActionBasket = async (req, res, next) => {

    try {

        let { account_id, basket_id } = req.params;
        let { data: { id: user_id } } = req.ctx.__userInfo;

        let sql = `
            select count(*) as nums from baskets where id = ? and account_id = (
                select account_id from account_user where user_id = ? and account_id = ?
            )
        `;
        let options = [ basket_id, user_id, account_id ];

        // user_id, account_id, basket_id 检查是否合法
        let [ { nums } ] = await QUERY(sql, options);
        if (!nums) return res.json(o4200);

    } catch (err){
        return next(err);
    }

    next();

};

module.exports.verifySuperRole = async (req, res, next) => {
    try {

        let { data: { id } } = req.ctx.__userInfo;
        let b = await Users.is_exists({ role: 9, id });
        if (!b) return res.json(o5400);

    } catch (err){
        return next(err);
    }

    next();
};

module.exports.relaUserActionAccount = async (req, res, next) => {


    try {

        let { user_id, account_id } = req.body;

        let b = await AccountUser.is_exists({ user_id, account_id });
        if (!b) return res.json(o5400);

    } catch (err){
        return next(err);
    }

    next();

};



