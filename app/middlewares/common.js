/**
 * @description
 *  通用中间件
 * 
 *  level: auto
 *      3xx         warn
 *      4xx, 5xx    error
 *      1xx, 2xx    info
 * 
 * @attribute
 *  ignore_reqs             需要屏蔽记录日志的请求
 *  loop_reqs               需要分离的写日志的请求
 * 
 */

const Express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const log4js = require("log4js");
const {
    logConfigure: { logger, runtime, loggerLoopWrite }
} = require("../libs");

const mw = Express.Router();

// 屏蔽记录的请求 - 一般都是轮询
const ignore_reqs = [
    {
        method: "get",
        api:  "/api/baskets/new/"
    },
    {
        method: "get",
        api: "/api/download/"
    },
    {
        method: "get",
        api: "/api/data/getOrders"
    },
    {
        method: "put",
        api: "/api/setAccountBalance/"
    },
    {
        method: "get",
        api: "/api/data/orders"
    },
    {
        method: "get",
        api: "/api/data/walletRecorded"
    }
];

// 需要记录的轮询请求
const loop_reqs = [
    {
        method: "put",
        api: "/api/orders/updateProcess/"
    }
];


// 记录接收到请求的时间
mw.use((req, res, next) => {
    req.__interface_started_at = Date.now();
    next();
});

mw.use(cors());
mw.use((req, res, next) => {
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
    
    next();
});

mw.use(bodyParser.urlencoded({ extended: false }));
mw.use(bodyParser.json());

// 记录来源ip
mw.use((req, res, next) => {

    // 来源ip
    let ip = req.headers["x-forwarded-for"] 
        ||  req.connection.remoteAddress 
        ||  req.socket.remoteAddress 
        ||  req.connection.socket.remoteAddress;

    // 确保单个 req.ctx 不可被覆盖
    Object.defineProperty(req, "ctx", {
        writable: false,
        configurable: false,
        enumerable: false,
        value: {}
    });

    if (ip) ([ ip ] = ip.split(","));
    req.ctx.__ip = ip;

    next();

});

// 统一传递来的数据, 并且拦截返回数据
mw.use((req, res, next) => {
    if(JSON.stringify(req.body) === "{}"){
        req.body = req.query;
    } else {
        req.body = Object.assign({}, req.body, req.query);
    }

    const sendJson = res.json;
    res.json = function (argv){
        res["__storage"] = argv;
        sendJson.bind(res)(argv);
    };

    next();
});
mw.use(log4js.connectLogger(logger, {
    level: "auto",
    format: (req, res, format) => {

        let method = req.method.toLowerCase();


        // 干掉部分接口的日志记录
        for (let item of ignore_reqs){
            if (~req.path.indexOf(item['api']) && method === item["method"]){
                console.log(`${new Date()} ${req.ctx.__ip} ${req.path}`);
                return ;
            }
        }

        // 干掉 http status 429
        if (res.statusCode === 429){
            console.log(`${new Date()} ${req.ctx.__ip} ${req.path}`);
            return ;
        }

        // 输出字段过滤
        let tmp = {};
        if (req.body){
            for (let [ k, v ] of Object.entries(req.body)){
                switch (k){
                    case "verifyToken":
                    case "password":
                    case "token":
                    continue;
                    default:
                    tmp[k] = v;
                }
            }
        }
        
        // res数据
        let { errcode } = res["__storage"];
        let resData = JSON.stringify({ errcode });
        delete res["__storage"];

        // req记录token内的用户数据
        if (req.ctx && req.ctx.__userInfo){
            tmp["userInfo"] = req.ctx.__userInfo["data"];
        }

        // 输出格式
        let s = [
            `"ip ${req.ctx.__ip}"`,
            `":method ${req.path}"`,
            `-rq ${JSON.stringify(tmp)}`,
            `-hs :status`,
            `-tc ${Date.now() - req.__interface_started_at}ms`,
            `-rs ${resData}`
        ];
        let log = format(s.join(" "));

        // 轮询写分离
        for (let api_param of loop_reqs){
            if (~req.path.indexOf(api_param['api']) && method === api_param["method"]){
                return loggerLoopWrite.info(log);
            }
        }

        // 读写分离
        if (method === "get"){
            return runtime.info(log);
        }

        return log;
    }
}));

module.exports = mw;
