/**
 * @description
 *  防护
 * 
 * @attribute
 *      ip_white                ip白名单
 *      ip_black                ip黑名单
 * 
 * 
 * @function
 *      access_restrictions     访问限制
 * 
 * 
 * 
 */

const redis = require("../services/redisConnect");
const { o5900, o5902 } = require("../result");


const ip_white = [
    "::1"
];

const ip_black = [

];


// 1小时为界
let times = 60 * 60 * 1;
let max = 2500;

// 研发环境 5分钟 100次
if (process.env.DEBUG){
    times = 5 * 60;
    max = 100;
}

module.exports.access_restrictions = async (req, res, next) => {
    let { ctx: { __ip } } = req;

    // 黑名单
    let b_black = ip_black.includes(__ip);
    if (b_black) {
        return res.status(403).json(o5902);
    }       

    // 白名单
    let b_white = ip_white.includes(__ip);
    if (b_white) return next();

    // 记录
    let s = `${__ip}-visit`;
    let ret = await redis.ttl(s);
    if (ret === -2){
        await redis.setex(s, times, 1);
    } else {
        await redis.incr(s);
    }
    ret = await redis.get(s);
    if (ret > max){
        return res.status(429).json(o5900);
    } 
   
    next();
};


