/**
 * @description
 *  私有的业务逻辑封装 ...
 * 
 * @function
 *  getBasketSendEmail              获取篮子及下面数据, 用于发送邮件
 *  __getAccountBalanceToOrders     给订单找到对应的余额
 * 
 */

const { QUERY } = require("../services/mysqlConnect");
const {
    models: { Baskets, Accounts }
} = require("../models");
const {
    formatEmail: { formatEmailAddress, formatBasketProgress },
} = require("../libs");
const { currencySendMail } = require("../services/diyEmail");



const __getAccountBalanceToOrders = async (account_id, basket) => {
    // 查询余额信息
    let { balance } = await Accounts.once({ id: account_id }, "balance");
    if (balance){
        balance = JSON.parse(balance);

        // 转换 [ {asset:"", balance: ""}, {} ] ==> { asset: balance }
        let tmp_balance = {};
        for (let item of balance){
            tmp_balance[item["asset"]] = item["balance"];
        }

        // order.balance = value || undefined
        for (let order of basket){
            if (order["base_asset"] in tmp_balance) order["balance"] = tmp_balance[order["base_asset"]];
        }
    }

    return basket;
};

/**
 *  
 * @param {*} basket_id             篮子id
 * @param {*} email_name            邮件小标题
 * @param {*} b_send_user           是否需要抄送用户
 */
const __getBasketAndSendEmail = async (basket_id, email_name, b_send_user = true) => {

    // 获取orders列表
    let basket = await QUERY(`
        select * from orders_process o right join (
            select * from orders where basket_id = ?
        ) b on o.order_id = b.id
    `, basket_id); 

    // 获取 basket 所属的 account_id 及 basket-filename
    let { account_id, filename } = await Baskets.once({ id: basket_id }, [ "account_id", "filename"] );
    
    // 生成table
    basket = await __getAccountBalanceToOrders(account_id, basket);
    basket.name = filename;
    let html = formatBasketProgress([ basket ]);

    // 获取关联 account id 的 users
    let users = await QUERY(`
        select nickname, email from users u right join (
            select user_id from account_user where account_id = ?
        ) aud on u.id = aud.user_id
    `, account_id);

    // 获取 account id 的关联邮件
    let { display_name, mailbox } = await Accounts.once({ id: account_id }, [ "display_name", "mailbox" ]);
    let emails = [];
    if (b_send_user) emails = formatEmailAddress(users, mailbox);

    // 发送
    currencySendMail({
        subject: `${display_name} - ${email_name}`,
        html
    }, emails, account_id);

};

module.exports = {
    __getBasketAndSendEmail,
    __getAccountBalanceToOrders
};
