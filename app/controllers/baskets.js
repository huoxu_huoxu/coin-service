/**
 * @description
 *  篮子 - 交易集合
 *  
 * @function
 *  upload_basket               上传basket的excel文件, 状态是 wait
 *  getBaskers                  获取 account id 下所有篮子的近况 及 篮子内包含的交易数
 *  updateBasket                开放给小灰灰的, 更新篮子字段
 * 
 *  
 *  downloadBasketFile          下载篮子
 *  inquiryBasket               下载篮子的前置接口, 检测是否可下载
 *  webUpdateBasket             更新篮子字段
 *  webDeleteBasket             删除篮子
 * 
 * 
 * 
 */

const path = require("path");
const fs = require("fs");
const uuidv1 = require("uuid/v4");
// const ccxt = require("ccxt");

const {
    readfile: { parseFile },
    diyClass: { CreateHttpError }
} = require("../libs");

const { __getBasketAndSendEmail } = require("./__getDataAndSendEmail");

const { succ, o5000, o5002, o4100, o5100, o4202, o5200, o4008, o5202, o5204, o4204 } = require("../result");
const { QUERY, AFFAIR } = require("../services/mysqlConnect");
const {
    models: { Baskets, Orders, AccountUser, Accounts }
} = require("../models");

// 暂时使用本地文件, 之后切 redis ...
// const exchanges = require("../tasks/exchanges.json");

// 交易所确认
// class Exchange {

//     constructor (exchange){
//         let exchange_key = this.__map_exchange_key(exchange);
//         this.exchange = new ccxt[exchange_key]();
//     }

//     // 某交易所是否支持币币交易
//     support_currency (){
//         if (process.env.DEBUG){
//             return require("../../local/bian_coins.json")["symbols"];
//         }
//         return this.exchange.fetchMarkets();

//     }

//     __map_exchange_key (exchange){
//         return exchanges[exchange];
//     }

// }


exports.uploadBasket = async (req, res, next) => {

    try {

        let data = await parseFile(req.file.path);
        // 将csv第一行的标题行去除
        data.shift();
        if (!data.length){
            let err = new Error(o5002.msg);
            err.info = o5002;
            throw err;
        }

        let { account_id } = req.body;
        let { id } = req.ctx.__userInfo.data;
        let basket_name = req.file.basket_name;
        let filename = req.file.originalname.replace(".csv", "");

        let nums = await AccountUser.is_exists({ user_id: id, account_id });
        if (!nums) return res.json(o4100);

        // 验证上传交易的币币交易是否有效, 之后再说..., 之后直接扔redis里
        // ... 暂时先关闭
        // let { exchange } = await Accounts.once({ id: account_id }, "exchange");
        // if (exchange){

        //     console.log("验证上传文件 ....");

        //     // 交易所名称对ccxt关键字映射
        //     let ex;
        //     try {
        //         ex = new Exchange(exchange);
        //     } catch (err){
        //         let msg = `暂时不支持交易所 ${exchange}`;
        //         let error = new Error(msg);
        //         error.info = Object.assign({}, o5204, { msg });
        //         throw error;
        //     }
            
        //     // 开始校对币种交易
        //     let coins = await ex.support_currency();
        //     for (let item of data){
        //         let [ base_asset, quote_asset ] = item;

        //         verify_asset:
        //         {
        //             for (let v of coins){
        //                 v = v.info || v;
        //                 if (v["baseAsset"] === base_asset && v["quoteAsset"] === quote_asset){
        //                     break verify_asset;
        //                 }
        //             }

        //             // 存在不支持的币种交易
        //             let msg = `交易所不支持: ${base_asset}/${quote_asset} 交易`;
        //             let error = new Error(msg);
        //             error.info = Object.assign({}, o5202, { msg });
        //             throw error;
        //         }
                    
        //     }

        //     console.log("验证通过 ....");
        // }


        // 写入数据库
        let conn = await AFFAIR();
        await conn.next("begin");

        try {
            // 向baskets表插入一条数据
            let [ s, options ] = Baskets.__generate_insert({
                name: basket_name,
                status: "wait",
                account_id: +account_id,
                filename
            });
            let { value: { insertId: basket_id } } = await conn.next(s, options);

            // 向orders表插入N条数据
            for (let i=0; i<data.length; i++){

                let name = uuidv1();
                let [
                    base_asset,
                    quote_asset,
                    direction,
                    qty,
                    algo_name,
                    start_time,
                    end_time,
                    algo_param,
                    note
                ] = data[i];

                start_time = start_time === "NA" ? null : start_time;
                end_time = end_time === "NA" ? null : end_time;

                let [ s, options ] = Orders.__generate_insert({
                    name,
                    basket_id,
                    base_asset,
                    quote_asset,
                    direction,
                    qty: +qty,
                    algo_name,
                    start_time,
                    end_time,
                    algo_param,
                    note
                });

                await conn.next(s, options);
            }

            // 上述所有sql插入操作必须全成功
            conn.next("commit");

        } catch (err){
            await conn.next("rollback");
            err.info = o5100;
            throw err;
        }

        return res.json(succ);

    } catch (err){
        let error = new CreateHttpError(err.info || o5000, `上传文件失败, ${err.toString()}`);
        fs.unlinkSync(path.resolve(process.env.realPath, req.file.path));
        next(error);
    }

};


exports.getBaskers = async (req, res, next) => {

    try {

        let { 
            filename, 
            coin_name, 
            status,
            started_time, 
            ended_time,
            page_index = 1
        } = req.body;

        let { account_id } = req.params;

        let select_dict = { "discarded": 0, account_id };
        status && (select_dict["status"] = status);

        // 获取符合条件的篮子
        let page_size = 10, total = 0, cond, resul;
        let [ sql, options ] = Baskets.base_sql(select_dict, "*");

        if (filename){
            options.push(`%${filename}%`);
            sql += " and filename like ? ";
        }
        if (started_time){
            let started_at = new Date(started_time);
            options.push(started_at);
            sql += " and started_at >= ?";
        }
        if (ended_time){
            let ended_at = new Date(ended_time);
            options.push(ended_at);
            sql += " and ended_at <= ?";
        }

        // 追加倒叙分页
        cond = sql;
        total = await Baskets.total_nums(cond, options);

        sql += " order by id desc ";
        sql += " limit ?, ? ";
        page_index = +page_index;
        options.push( (page_index - 1) * page_size, page_size);

        resul = await QUERY(sql, options);
        

        // 获取篮子下面的交易数
        for (let item of resul){
            let [ sql, options ] = Orders.base_sql({ basket_id: item.id }, "count(*) as nums");
            
            if (coin_name){
                options.push(`%${coin_name}%`, `%${coin_name}%`);
                sql += ` and ( base_asset like ? or quote_asset like ? )`;
            }

            let [ { nums } ] = await QUERY(sql, options);
            item.order_count = nums;
        }
        // 过滤不包含 coin_name 的篮子, 只有在coin_name查询才有效
        coin_name && (resul = resul.filter(v => v.order_count > 0));

        
        return res.json(Object.assign({}, succ, {
            data: {
                list: resul,
                total,
                page_index,
                page_size
            }
        }));

    } catch (err){
        next(err);
    }

};


exports.updateBasket = async (req, res, next) => {

    try {

        let { basket_name } = req.params;

        let ret = await Baskets.once({ name: basket_name });
        if (!ret) return res.json(o4008);
        
        // update
        let { status } = req.body;
        let tmp = {};
        status && (tmp['status'] = status);

        await Baskets.update(tmp, { id: ret.id });

        return res.json(succ);

    } catch (err){
        next(err);
    }

};



exports.webUpdateBasket = async (req, res, next) => {

    try {

        let { basket_id } = req.params;
        let { do_opt, new_filename, status } = req.body;

        let tmp = {};

        // 动作
        if (do_opt){
            tmp['do_opt'] = do_opt;
            tmp['status'] = "pending";
            await Baskets.update(tmp, { id: basket_id });
        }

        // 重命名 
        if (new_filename){
            await Baskets.update({ filename: new_filename }, { id: basket_id });
            tmp["filename"] = new_filename;
        }

        // 状态
        if (status){
            let { affectedRows: rows } =  await Baskets.update({ status: "new" }, { id: basket_id, status: "wait" });
            if (!rows) return res.json(o4202);

            // 给公司系统抄送 用户开始运行了篮子
            __getBasketAndSendEmail(basket_id, "开始运行", false);
            tmp["status"] = "new";
        }

        // end
        tmp['basket_id'] = basket_id;
        return res.json(Object.assign({}, succ, { data: tmp }));
        
    } catch (err){
        next(err);
    }

};


exports.downloadBasketFile = async (req, res, next) => {

    try {

        let { basket_id } = req.params;

        let { name, filename } = await Baskets.once({ id: basket_id }, [ "name", "filename" ]);

        res.download(path.resolve(process.env.UPLOAD_PATH, `${name}.csv`), `${filename}.csv`, (err) => {
            if (err){   
                let error = new CreateHttpError(o5200, err.toString(), 404);
                next(error);
            } 
        });

    } catch (err){
        next(err);
    }

};


exports.inquiryBasket = async (req, res, next) => {

    try {

        let { basket_id } = req.params;

        // 数据表是否存在数据记录
        let ret = await Baskets.once({ id: basket_id }, "name");
        if (!ret) return res.json(o5200);

        // 文件是否存在
        let { name } = ret;
        if (!fs.existsSync(path.resolve(process.env.UPLOAD_PATH, `${name}.csv`))){
            return res.json(o5200);
        }

        return res.json(succ);

    } catch (err){
        next(err);
    }

};


exports.webDeleteBasket = async (req, res, next) => {

    try {

        let { basket_id } = req.params;

        // 更新字段 - discarded 废弃篮子
        let { status } = await Baskets.once({ id: basket_id }, "status");
        if (status !== "finished" && status !== "wait" && status !== "stopped") return res.json(o4204);

        await Baskets.update({ discarded: 1 }, { id: basket_id });

        return res.json(Object.assign({}, succ, { data: { basket_id } }));

    } catch (err){
        next(err);
    }

};

