/**
 * @description
 *  交易单
 * 
 * @function
 *  update_orders           更新 orders 交易进度
 * 
 * @TODO
 *  之后前端接口修改orders表, user_pov不能为0, 0代表自动过滤
 * 
 */

const Iconv = require('iconv').Iconv;
const { QUERY } = require("../services/mysqlConnect");
const { 
    __getBasketAndSendEmail,
    __getAccountBalanceToOrders
} = require("./__getDataAndSendEmail");
const {
    models: { Orders, OrdersProcess, Baskets, Accounts }
} = require("../models");
const {
    tools: { getCurrentDate },
    logConfigure: { loggerErr }
} = require("../libs");
const { succ, o4002, o4008, o4012 } = require("../result");


exports.updateOrders = async (req, res, next) => {

    try {
        let { data } = req.body;
        let { basket_name } = req.params;

        let ret = await Baskets.once({ name: basket_name }, [ "id", "filename", "status" ]);
        if (!ret) return res.json(o4008);
        let { id: basket_id, status } = ret;

        // 篮子结束后, 不允许修改内部订单
        if (status === "finished") return res.json(o4012);

        let err_name = [];
        for (let i=0; i<data.length; i++){

            let { trade, current_algo_param, status, error_msg, average_price, name } = data[i];
            let item = await Orders.once({ name, basket_id }, "qty, id");

            // 防止 current_algo_param 为 null, 影响后面编字符串
            current_algo_param = current_algo_param || {};

            // 防止出现 更新不存在的 order - name
            if (!item) {
                err_name.push(name);
                continue;
            }

            let notrade = 0;
            let pct = 0;
            if (item.qty){
                pct = ( trade / item.qty * 100).toFixed(2);
                notrade = item.qty - trade;
            }

            let order_id = item["id"];
            let nums = await OrdersProcess.is_exists({ order_id });

            error_msg = error_msg || "";
            let obj = {
                trade: +trade,
                pct: +pct,
                current_algo_param: JSON.stringify(current_algo_param),
                average_price: Number(average_price),
                status,
                error_msg,
                notrade
            };
            if (nums){
                await OrdersProcess.update(obj, {
                    order_id
                });
            } else {
                obj['order_id'] = order_id;
                await OrdersProcess.insert(obj);
            }

        }

        //  失败的order_id数小于需要更新的数目, 认为已经有成功的了, 触发检查篮子内的所有交易是否都已经完成
        if (err_name.length !== data.length){
            let resul = await Orders.all({ basket_id }, "id");
            resul = resul.map(item => item.id);

            let s = resul.join(","), finished_nums;
            [ { finished_nums } ] = await QUERY(
                `select count(*) as finished_nums from orders_process where order_id in (${s})`
            );

            if (finished_nums === resul.length){

                // 需要优化 ...
                [ { finished_nums } ] = await QUERY(
                    `select count(*) as finished_nums from orders_process where order_id in (${s}) and status != ?`,
                    [ "finished" ]
                );

                if (!finished_nums){

                    Baskets.update({ 
                        status: "finished",
                        ended_at: new Date()
                    }, {
                        id: basket_id
                    });

                    // 发送 - 完结报告 ...
                    __getBasketAndSendEmail(basket_id, "完结报告");
                }

            }

        }

        if (err_name.length){
            return res.json(Object.assign({}, o4002, {
                data: {
                    err_name: err_name
                }
            }));
        }
        
        return res.json(Object.assign({}, succ));

    } catch(err){
        next(err);
    }

};


exports.getOrders = async (req, res, next) => {

    try {

        let { basket_name } = req.params;
        
        let ret = await Baskets.once({ name: basket_name }, "*");
        if (!ret) return res.json(o4008);

        let basket_id = ret["id"];
        let resul = await Orders.all({ basket_id }, "*");

        // 返回json对象 非json字符串, 不重复 涉及篮子的运行状态
        if (resul){
            resul = resul.map(v => {
                v.user_algo_param = JSON.parse(v.user_algo_param);
                return v;
            });
        }

        return res.json(Object.assign({}, succ, {
            data: {
                orders: resul,
                basket: ret
            }
        }));

    } catch (err){
        next(err);
    }

};

exports.webGetOrders = async (req, res, next) => {

    try {

        let { coin_name, status } = req.body;
        let { account_id, basket_id } = req.params;
        let { id } = req.ctx.__userInfo.data;

        let sql = `
            select * from orders o join (
                select id as basket_id from baskets b join (
                    select account_id from account_user where user_id = ? and account_id = ?
                ) a on a.account_id = b.account_id where b.id = ?
            ) v on v.basket_id = o.basket_id 
        `;
        let options = [ id, account_id, basket_id ];

        // 搜索条件
        let cond = [];
        if (coin_name){
            cond.push(" (o.base_asset like ? or o.quote_asset like ?) ");
            options.push(`%${coin_name}%`, `%${coin_name}%`);
        }
        if (cond.length){
            sql += ` where ${cond.join(" and ")}`;
        }
        let resul = await QUERY(sql, options);

        // orders_process
        if (resul.length){
            if (status){

                let ids = resul.map(v => v.id);
    
                // 查询条件
                let sql = "select * from orders_process where ";
                let cond = [], options = [];
    
                // new 单独处理
                if (status !== "new"){
                    cond = [ " status = ? " ];
                    options.push(status);
                }
                cond.push(` order_id in (${Array(ids.length).fill("?")}) `);
                options = options.concat(ids);
                sql += ` ${cond.join("and")} `;
    
                // 进度挂在 process 上
                let ret = await QUERY(sql, options);
                for (let item of ret){
                    for (let t of resul){
                        if (item.order_id === t.id) {
                            t.process = item;
                        }
                    }
                }
    
                // 无进度的 -> new, 其余则是各种状态的
                if (status !== "new"){
                    resul = resul.filter(v => !!v.process);
                } else {
                    resul = resul.filter(v => {
                        return !v.process;
                    });
                }
    
            } else {
                for (let item of resul){
                    let ret = await OrdersProcess.once({ order_id: item.id }, "*");
                    item.process = ret || null;
                }
            }
    
            // 查询余额信息
            resul = await __getAccountBalanceToOrders(account_id, resul);
            resul = resul.map(v => {
                if (!v.balance){
                    v.balance = 0;
                }
                return v;
            });
        }

        // basket status
        let basket = await Baskets.once({ id: basket_id }, "*");
        
        return res.json(Object.assign({}, succ, {
            data: {
                list: resul,
                basket
            }
        }));

    } catch (err){
        next(err);
    }

};


exports.webUpdateOrder = async (req, res, next) => {

    try {

        let { order_id } = req.params;
        let { do_opt, user_algo_param, qty } = req.body;

        let tmp = {};
        if (do_opt){
            tmp['do_opt'] = do_opt;
            await OrdersProcess.update({ status: "pending" }, { order_id });
        }

        qty && (tmp['qty'] = +qty);

        if (user_algo_param){
            tmp["user_algo_param"] = JSON.stringify(user_algo_param);
        }

        await Orders.update(tmp, { id: order_id });
        res.json(succ);

    } catch (err){
        next(err);
    }

};

exports.webExportOrders = async (req, res, next) => {

    try {
        
        let { account_id, started_at, ended_at, is_answer } = req.body;

        // 应答 - 确认是否可以导出
        if (is_answer) return res.json(succ);

        // get 字符串 转 数字
        started_at = +started_at;
        ended_at = +ended_at;

        // 获取一段时间内所有的非 wait / new 的篮子内的所有订单
        let sql = `
            select bids.filename, bids.name, op.trade, op.current_algo_param, qty, base_asset, quote_asset, direction, o.updated_at, o.created_at, o.id from orders o join (
                select id, filename, name from baskets where status not in ("new", "wait") and account_id = ? and created_at > ? and updated_at < ?
            ) bids on o.basket_id = bids.id left join orders_process op on o.id = op.order_id
        `;
        let options = [+account_id, new Date(started_at), new Date(ended_at) ];
        let resul = await QUERY(sql, options);


        // 获取账户所在交易所
        let account = await Accounts.once({id: account_id}, "exchange");

        // 整理数据
        if (account.exchange === "Bitmex"){
            resul =  resul.map(function(value){

                let position_change;
                try {
                    position_change = JSON.parse(value["current_algo_param"])["position_change"];
                } catch (err){
                    loggerErr.error("超管权限, 导出时发生错误", err.toString());
                }

                let arr = [
                    value.filename,
                    `${value.base_asset}/${value.quote_asset}`,
                    value.direction,
                    value.qty,
                    value.trade || 0,
                    position_change || 0,
                    value.name,
                    getCurrentDate(value.created_at).slice(0, 3).join("-"),
                    getCurrentDate(value.updated_at).slice(0,3).join("-")
                ];
                return arr.join(",");
            });

            resul.unshift("名称,币种,交易,总量,成交量,变动,编号,开始时间,结束时间");
        } else {
            resul =  resul.map(function(value){
                let arr = [
                    value.filename,
                    `${value.base_asset}/${value.quote_asset}`,
                    value.direction,
                    value.qty,
                    value.trade || 0,
                    value.name,
                    getCurrentDate(value.created_at).slice(0, 3).join("-"),
                    getCurrentDate(value.updated_at).slice(0,3).join("-")
                ];
                return arr.join(",");
            });
            resul.unshift("名称,币种,交易,总量,成交量,编号,开始时间,结束时间");
        }
        
        // csv 文件头
        let { display_name } = await Accounts.once({ id: +account_id }, "display_name");
        let csv_name = `${display_name}_orders_${getCurrentDate(started_at).slice(0,3).join("_")}_${getCurrentDate(ended_at).slice(0,3).join("_")}`;
        res.attachment(`${csv_name}.csv`);

        // 防止缓存, 不提示用户保存文件
        res.setHeader("Pragma", "No-cache"); 
        res.setHeader("Cache-Control", "No-cache"); 
        res.setHeader("Expires", 0);

        // 转换编码格式, 兼容 windows Excel 打开情况
        let iconv = new Iconv('UTF-8', 'GBK');
        let buf = iconv.convert(resul.join("\n"));
        res.send(buf);

    } catch (err){
        next(err);
    }

};

