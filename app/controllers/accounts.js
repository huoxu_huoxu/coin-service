/**
 * @description
 *  代理账号
 * 
 * @function
 *  addAccount                  添加 account
 *  getAccountRelationBaskets   获取代理账号下的新的篮子
 *  getOrdersRelationAB         获取代理账号下某篮子的全信息
 *  setAccountBalance           设置账户余额信息
 * 
 *  getAccounts                 业务级接口: 获取用户下所有account信息及对应运行中的篮子个数
 * 
 *  webExportAccountBalance     导出账户仓位
 * 
 *  webExportSettlementPrice    超管权限, 导出仓位快照及结算价
 * 
 */

const Iconv = require('iconv').Iconv;
const {
    models: { Users, Accounts, AccountUser, Baskets, Orders }
} = require("../models");
const { succ, o4000, o4004, o4006, o5100, o4020, o5210, o9000, o1014 } = require("../result");
const {
    tools: { test, getCurrentDate },
    logConfigure: { loggerErr },
    runtimeCompatible: { getBinancePairsMap, calculateSettlementPrice }
} = require("../libs");
const { QUERY, AFFAIR } = require("../services/mysqlConnect");


exports.addAccount = async (req, res, next) => {

    try {

        let { display_name, name, user_id, exchange } = req.body;

        let nums = await Users.is_exists(user_id);
        if (!nums) return res.json(o4004);

        let conn = await AFFAIR();
        await conn.next("begin");

        try {

            // 向accounts表新增一条数据
            let [ s, options ] = Accounts.__generate_insert({
                display_name,
                name,
                exchange: exchange || ""
            }, true);
            let { value: { insertId: account_id } } = await conn.next(s, options);

            // 向account_user表新增一条数据
            [ s, options ] = AccountUser.__generate_insert({
                user_id,
                account_id
            }, true);
            await conn.next(s, options);
            
            // 上述两条sql必须全部成功
            await conn.next("commit");

        } catch (err){
            await conn.next("rollback");
            return res.json(Object.assign({}, o5100, {
                msg: err.toString()
            }));
        }

        return res.json(succ);

    } catch (err){
        next(err);
    }

};

exports.getAccountRelationBaskets = async (req, res, next) => {

    try {
        let { account_name } = req.params;

        let account = await Accounts.once({ name: account_name });
        if (!account) return res.json(o4006);

        let resul = await Baskets.all({
            account_id: account['id'],
            status: "new"
        }, [ "name" ]);

        for (let v of resul){
            v.account_name = account_name;
            v.basket_name = v.name;
            delete v['name'];
        }

        return res.json(Object.assign({}, succ, {
            data: {
                baskets: resul
            }
        }));
        
    } catch (err){
        next(err);
    }

};

exports.getOrdersRelationAB = async (req, res, next) => {

    try {

        let { account_name, basket_name } = req.params;

        // 获取 account-name 对应 id
        let ret = await Accounts.once({ name: account_name });
        if (!ret) return res.json(o4006);

        // account id 是否关联了 basket name
        ret = await Baskets.once({ account_id: ret['id'], name: basket_name }, "*");
        if (!ret) return res.json(Object.assign({}, o4000, {
            msg: `account_name: ${account_name} 下不存在 basket_name: ${basket_name}`
        }));
        if (ret.status !== "new") return res.json(o5210);

        // 通过 basket id 获取 orders 表所有的订单
        let resul = await Orders.all({ basket_id: ret['id'] }, "*");
        resul = resul.map(v => {
            if (v.user_algo_param) v.user_algo_param = JSON.parse(v.user_algo_param);
            return v;
        });

        // 更新 basket id 运行状态
        await Baskets.update({
            status: "running",
            started_at: new Date()
        }, {
            name: basket_name
        });

        return res.json(Object.assign({}, succ, { data: { orders: resul } }));

    } catch (err){
        next(err);
    }

};


exports.getAccounts = async (req, res, next) => {

    try {

        let { id } = req.ctx.__userInfo.data;
        
        let s = `
            select * from accounts as a join 
            ( select au.account_id as id from account_user as au where user_id = ?) o 
            on a.id = o.id
        `;
        let resul = await QUERY(s, [ id ]);

        if (resul.length){
            for (let item of resul){
                let [ sql, options ] = Baskets.base_sql({ account_id: item.id, discarded: 0 }, [ "status", "count(*) as num" ]);
                let groups = await QUERY(sql + "group by status", options);

                let count = 0, running_count = 0;
                for (let group of groups){
                    count += group["num"];
                    if (!running_count){
                        if (group["status"] === "running"){
                            running_count = group["num"];
                        }
                    }
                }
                item.running_count = running_count;
                item.count = count;
            }
        }

        let ret, errObj;
        [ret, errObj] = await getBinancePairsMap();
        if (ret !== undefined){
            let pairs = new Map();
            for(let item of ret){
                pairs.set(item.symbol, item.price);
            }
            for (let i=0;i<resul.length;i++) {
                let account = resul[i];
                if(account.balance) {
                    let balances = JSON.parse(account.balance);
                    balances = calculateSettlementPrice(balances, pairs);
                    resul[i].balance = JSON.stringify(balances);
                } 
            }
        } else {
            loggerErr.warn("get binance pairs map", "https://api.binance.com/api/v3/ticker/price", errObj);
        }

        // 暂时不拆分了, 先放着
        // let resul = await AccountUser.all({ user_id: id }, "account_id as id");
        // if (resul.length){
        //     for (let account of resul){
        //         let ret = await Accounts.once({ id: account.id }, "*");
        //         Object.assign(account, ret);

        //         let [ sql, options ] = Baskets.base_sql({ account_id: account.id, discarded: 0 }, [ "status", "count(*) as num" ]);
        //         let groups = await QUERY(sql + "group by status", options);

        //         let count = 0, running_count = 0;
        //         for (let group of groups){
        //             count += group["num"];
        //             if (!running_count){
        //                 if (group["status"] === "running"){
        //                     running_count = group["num"];
        //                 }
        //             }
        //         }
        //         account.running_count = running_count;
        //         account.count = count;
        //     }
        // }

        return res.json(Object.assign({}, succ, { data: { list: resul } }));
    } catch (err){
        next(err);
    }

};

exports.setAccountBalance = async (req, res, next) => {

    try {

        let { account_name } = req.params;
        let { list } = req.body;

        let item = await Accounts.once({ name: account_name }, [ "id", "balance" ]);
        if (!item) return res.json(o4006);

        if (typeof list !== "string"){
            if (!test.isArray(list)) return res.json(o4020);
            list = JSON.stringify(list);
        }

        // no real time requirement
        if (list !== item.balance) {
            Accounts.update({ "balance": list }, { id: item.id });
        } else {
            Accounts.update({  }, { id: item.id });
        }

        return res.json(succ);

    } catch (err){
        next(err);
    }

};


exports.webExportAccountBalance = async (req, res, next) => {
    try {
        let { is_answer } = req.body;
        let { account_id } = req.params;
        let { data: { id } } = req.ctx.__userInfo;
        let nums = await AccountUser.is_exists({ user_id: id, account_id: account_id });

        if (is_answer) {
            if (nums > 0) {
                return res.json(succ);
            } else {
                return res.json(o1014);
            }
        }
        
        if (nums > 0) {
            let sql = `
                select balance from accounts where id = ?
            `;
            let options = [ account_id ];
            let resul = await QUERY(sql, options);

            let aCsv = [];
            let sBalance = resul[0].balance;
            if (!(sBalance === "[]" || !sBalance || sBalance  === "")) {
                let balance = JSON.parse(sBalance);
                for(let coin of balance) {
                    let tmp = [];
                    tmp.push(coin.asset, coin.balance);
                    aCsv.push(tmp.join(","));
                }
                aCsv.unshift(["asset", "balance"].join(","));
            }

            let sCsv = aCsv.join("\n");
            // csv 文件头
            let { display_name } = await Accounts.once({ id: +account_id }, "display_name");
            let csv_name = `${display_name}_balance_${getCurrentDate(Date.now()).slice(0,3).join("_")}`;
            res.attachment(`${csv_name}.csv`);

            // 防止缓存, 不提示用户保存文件
            res.setHeader("Pragma", "No-cache"); 
            res.setHeader("Cache-Control", "No-cache"); 
            res.setHeader("Expires", 0);

            // 转换编码格式, 兼容 windows Excel 打开情况
            let iconv = new Iconv('UTF-8', 'GBK');
            let buf = iconv.convert(sCsv);
            res.send(buf);
        } else {
            return res.json(o9000);
        }

    } catch (err){
        next(err);
    }
};

exports.webExportSettlementPrice = async (req, res, next) => {
    try {
        let { account_id, started_at, ended_at, is_answer } = req.body;
        if (is_answer) {
            if (process.env.DEBUG){
                if(account_id !== "1") {
                    return res.json(o9000);
                }
            } 
            // else {
            //     if(account_id !== "25" && account_id !== "10" && account_id !== "26" && account_id !== "40" && account_id !== "42") {
            //         return res.json(o9000);
            //     }
            // }
            return res.json(succ);
        }

        // get 字符串 转 数字
        started_at = +started_at;
        ended_at = +ended_at;

        let sql = `
            select settlement_price, pos, created_at from wallet_recorded where account_id = ? and created_at >= ? and created_at <= ?
        `;
        let options = [ +account_id, new Date(started_at), new Date(ended_at) ];
        let resul = await QUERY(sql, options);

        let tmp = [];
        for (let item of resul){
            let oDate = new Date(item.created_at);
            if (process.env.DEBUG){
                if (oDate.getSeconds() === 8) {
                    tmp.push(item);
                }
            } else {
                if (oDate.getHours() === 8) {
                    tmp.push(item);
                }
            }
        }
        resul = tmp;

        let poses = [], sps = [];
        for (let item of resul) {
            let aDate = getCurrentDate(new Date(item.created_at).getTime());
            let sDate = aDate.splice(0, 3).join("-") + " " + aDate.join(":");

            let aTmp = ["datetime," + sDate];
            let balances = JSON.parse(item.pos);
            for (let bs of balances){
                aTmp.push(`${bs.asset},${bs.balance}`);
            }

            poses.push(aTmp.join("\n"));
            sps.push(`${sDate}, ${item.settlement_price}`);
        }

        sps.unshift("datetime, settlement price (USDT计价)");
        let sCsv = poses.join("\n\n") + "\n\n\n" + sps.join("\n");

        // csv 文件头
        let { display_name } = await Accounts.once({ id: +account_id }, "display_name");
        let csv_name = `${display_name}_settlement_${getCurrentDate(started_at).slice(0,3).join("_")}_${getCurrentDate(ended_at).slice(0,3).join("_")}`;
        res.attachment(`${csv_name}.csv`);

        // 防止缓存, 不提示用户保存文件
        res.setHeader("Pragma", "No-cache"); 
        res.setHeader("Cache-Control", "No-cache"); 
        res.setHeader("Expires", 0);

        // 转换编码格式, 兼容 windows Excel 打开情况
        let iconv = new Iconv('UTF-8', 'GBK');
        let buf = iconv.convert(sCsv);
        res.send(buf);
    }catch (err){
        next(err);
    }
};

