/**
 * @description
 *  返回 errcode 集合
 *  
 * @desc
 *  o1xxx                验证类错误
 * 
 *  o4xxx                请求端错误
 *  
 *  o5xxx                服务器端写入错误
 * 
 *  o9xxx                权限
 * 
 */


const verifyError = {
    o1000: {
        errcode: 1000,
        msg: "email已注册过, 请勿重复注册"
    },
    o1002: {
        errcode: 1002,
        msg: "签证失败"
    },
    o1004: {
        errcode: 1004,
        msg: "验证图片过期"
    },
    o1006: {
        errcode: 1006,
        msg: "验证码错误"
    },
    o1008: {
        errcode: 1008,
        msg: "账号 / 邮箱 不存在"
    },
    o1010: {
        errcode: 1010,
        msg: "账号或密码错误"
    },
    o1012: {
        errcode: 1012,
        msg: "非法注册"
    },
    o1014: {
        errcode: 1014,
        msg: "非法操作"
    },
    o1016: {
        errcode: 1016,
        msg: "不存在用户"
    },
    o1100: {
        errcode: 1100,
        msg: "登录过期"
    },
    o1102: {
        errcode: 1102,
        msg: "请先登录, 在进行相关操作"
    }
};

const frontError = {
    o4000: {
        errcode: 4000,
        msg: "account_name下没这个basket_name"
    },
    o4002: {
        errcode: 4002,
        msg: "部分order name不存在"
    },
    o4004: {
        errcode: 4004,
        msg: "用户id不存在"
    },
    o4006: {
        errcode: 4006,
        msg: "account name 不存在"
    },
    o4008: {
        errcode: 4008,
        msg: "无效的 basket name"
    },
    o4012: {
        errcode: 4012,
        msg: "已经结束的篮子, 不允许修改篮子内的订单"
    },
    o4020: {
        errcode: 4020,
        msg: "数据格式不正确, 请检查"
    },
    o4100: {
        errcode: 4100,
        msg: "非法上传"
    },
    o4200: {
        errcode: 4200,
        msg: "非法操作"
    },
    o4202: {
        errcode: 4202,
        msg: "无法修改状态非wait的篮子"
    },
    o4204: {
        errcode: 4204,
        msg: "无法删除在运行中的篮子"
    }
};

const serviceError = {
    o5000: {
        errcode: 5000,
        msg: "文件解析错误, 请检查文件格式是否是标准csv文件"
    },
    o5002: {
        errcode: 5002,
        msg: "篮子内订单数量为0"
    },
    o5100: {
        errcode: 5100,
        msg: "运行事务级sql失败"
    },
    o5200: {
        errcode: 5200,
        msg: "需要下载的文件不存在或缺失, 无法下载"
    },
    o5202: {
        errcode: 5202,
        msg: "交易所存在不能交易的"
    },
    o5204: {
        errcode: 5204,
        msg: "暂不支持交易所"
    },
    o5210: {
        errcode: 5210,
        msg: "处于非new状态, 不可调用"
    },
    o5400: {
        errcode: 5400,
        msg: "超出用户权限, 服务器拒绝访问"
    },
    o5900: {
        errcode: 5900,
        msg: "请求过于频繁"
    },
    o5902: {
        errcode: 5902,
        msg: "服务器拒绝访问"
    }
};

const permissionError = {
    o9000: {
        errcode: 9000,
        msg: "权限不够"
    },
   
};

const resul = Object.assign({
    succ: {
        errcode: 0,
        msg: "succ" 
    }
}, verifyError, frontError, serviceError, permissionError);

module.exports = resul;
