/**
 * @description
 *  表的模型实例
 * 
 * @default
 *  type string minLength 6, maxLength 255
 * 
 */

const {
    Model
} = require("../libs");

const Users = new Model("users", {
    parent_id: {
        type: "number"
    },
    nickname: {
        type: "string",
        required: true
    },
    email: {
        type: "string",
        required: true
    },
    password: {
        type: "string",
        required: true
    },
    salt: {
        type: "string",
        required: true,
        minLength: 1
    },
    role: {
        type: "number"
    }
});

const Accounts = new Model("accounts", {
    display_name: {
        type: "string",
        required: true
    },
    name: {
        type: "string",
        required: true
    },
    exchange: {
        type: "string",
        required: true,
        minLength: 2
    }
});

const AccountUser = new Model("account_user", {
    user_id: {
        type: "number",
        required: true
    },
    account_id: {
        type: "number",
        required: true
    }
});

const Baskets = new Model("baskets", {
    name: {
        type: "string",
        required: true
    },
    filename: {
        type: "string",
        required: true
    },
    status: {
        type: "string",
        required: true,
        minLength: 1,
        maxLength: 16
    },
    account_id: {
        type: "number",
        required: true
    }
});

const Orders = new Model("orders", {
    basket_id: {
        type: "number",
        required: true
    },
    base_asset: {
        type: "string",
        minLength: 1,
        required: true
    },
    quote_asset: {
        type: "string",
        minLength: 1,
        required: true
    },
    direction: {
        type: "string",
        minLength: 1,
        maxLength: 100,
        required: true
    },
    algo_name: {
        type: "string",
        minLength: 1,
        required: true
    },
    algo_param: {
        type: "string",
        minLength: 1,
        maxLength: null,
        required: true
    },
    start_time: {
        type: "string"
    },
    end_time: {
        type: "string"
    },
    qty: {
        type: "number",
        required: true
    },
    do_opt: {
        type: "string"
    },
    note: {
        type: "string",
        minLength: 1,
        maxLength: null
    },
    name: {
        type: "string",
        required: true
    },
    user_algo_param: {
        type: "string"
    }
});
const OrdersProcess = new Model("orders_process", {
    order_id: {
        type: "number",
        required: true
    },
    trade: {
        type: "number",
        required: true
    },
    notrade: {
        type: "number",
        require: true
    },
    pct: {
        type: "number",
        required: true
    },
    status: {
        type: "string",
        maxLength: 100,
        required: true
    },
    error_msg: {
        type: "string",
        required: true
    },
    current_algo_param: {
        type: "string",
        maxLength: 2048,
        required: true
    },
    average_price: {
        type: "number",
        required: true
    }
});

const EmailTasks = new Model("email_tasks", {
    user_id: {
        type: "number",
        required: true
    },
    step: {
        type: "number",
        required: true
    },
    accounts: {
        type: "string"
    },
    status: {
        type: "string"
    },
    disabled: {
        type: "boolean"
    }
});

const WalletRecorded = new Model("wallet_recorded", {
    account_id: {
        type: "number",
        required: true
    },
    settlement_price: {
        type: "number",
        required: true
    },
    pos: {
        type: "string",
        required: true,
        minLength: 1,
        maxLength: null,
    }
})


module.exports = {
    Users,
    Accounts,
    AccountUser,
    Baskets,
    Orders,
    OrdersProcess,
    EmailTasks,
    WalletRecorded
};

