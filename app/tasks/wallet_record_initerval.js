/**
 * @descritpion
 *  定时记录仓位信息及结算价
 *  
 * 
 */

const {
    logConfigure: { loggerWalletRecord },
    runtimeCompatible: { getBinancePairsMap, calculateSettlementPrice }
} = require("../libs");
const { Accounts, WalletRecorded } = require("../models/models");

const wallet_record = async () => {
    let ret, errObj;
    [ret, errObj] = await getBinancePairsMap();
    if (ret === undefined) {
        loggerWalletRecord.warn("wallet record get binance pairs map error", errObj);
        return ;
    }
    let pairs = new Map();
    for(let item of ret){
        pairs.set(item.symbol, item.price);
    }

    let accounts = await Accounts.all({}, [ "id", "balance" ]);
    for (let account of accounts){
        let id;
        if (! (account.balance === "" || account.balance === "[]" || !account.balance)) {
            let balances = JSON.parse(account.balance);
            balances= calculateSettlementPrice(balances, pairs);
            
            let total = 0;
            for (let v of balances){
                if(v["usdt"] !== "N/A"){
                    total += v["usdt"]
                }
            }
            
            id = await WalletRecorded.insert({ account_id: account.id, settlement_price: total,pos: account.balance });
        } else {
            id = await WalletRecorded.insert({ account_id: account.id, settlement_price: 0,pos: "[]" });
        }
        loggerWalletRecord.info("insert id", id);
    }
};


module.exports = () => {
    const iCycleSecond = 8 * 60 * 60;
    const getV = () => {
        if (process.env.DEBUG) {
            return 600000;
        } else {
             let t = Date.now()/1000;
            return Math.ceil((iCycleSecond - t % (24 * 60 * 60) % iCycleSecond) * 1000);
        }
    };

    // 还有个办法, 直接根据秒算
    const countdown = (ms) => {
        setTimeout(() => {
            wallet_record();
            return countdown(getV());
        }, ms);
    };
    
    // 正常情况下分钟级别查询, 如果整点不是7,8转小时级别赚小时级别查询
    let t = getV();
    countdown(t);

    loggerWalletRecord.info("启动 wallet record - tasks, countdown %d", t);
};






