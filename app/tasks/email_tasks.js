/**
 * @description
 *  发送邮件 - 报告篮子进度
 * 
 * 
 */

const { 
    cipher: { 
        createMd5 
    },
    logConfigure: {
        loggerEmailTask
    },
    formatEmail: {
        formatBasketProgress,
        formatEmailAddress
    }
} = require("../libs");
const { EmailTasks, Accounts } = require("../models/models");
const { QUERY } = require("../services/mysqlConnect");
const { currencySendMail } = require("../services/diyEmail");

// 一分钟检索一次邮件任务表
const times = 60 * 1000;

/**
 * @desc
 *  定时器记录对象
 *  
 *  tasks               建立任务
 *  get_orders          获取需要的订单, 按照篮子分类
 *  send_email          发送邮件
 * 
 *  account_id {
 *      timer           定时器
 *      ci              md5(条件)
 *      untimer         解除
 *  }
 *  
 * 
 */

const timers = new Map();
const reg_email = /\w+@.+\..{1,5}/;


const send_email = async (obj_baskets, display_name, to_email, account_id) => {

    // 查询余额信息
    let { balance } = await Accounts.once({ id: account_id }, "balance");
    if (balance){
        balance = JSON.parse(balance);

        // 转换 [ {asset:"", balance: ""}, {} ] ==> { asset: balance }
        let tmp_balance = {};
        for (let item of balance){
            tmp_balance[item["asset"]] = item["balance"];
        }

        // order.balance = value || undefined
        for (let basket of Object.values(obj_baskets)){
            for (let order of basket){
                if (order["base_asset"] in tmp_balance) order["balance"] = tmp_balance[order["base_asset"]];
            }
        }
    
    }
    
    let html = formatBasketProgress(obj_baskets);
    currencySendMail({
        subject: `${display_name} - 交易进度`,
        html
    }, to_email, account_id);
};


const get_orders = async ({ account_id, status }) => {

    // 邮箱是否合法
    let user_sql = `
        select u.email, u.nickname, u.id, a.display_name, a.mailbox from users u join (
            select user_id, account_id from account_user where account_id = ?
        ) p on p.user_id = u.id join accounts a on p.account_id = a.id
    `;
    let user_options = [ account_id ];
    let users = await QUERY(user_sql, user_options);
    if (!users || !users.length) return loggerEmailTask.error("没有关联 account 的 user", account_id);
    users = users.filter(user => {
        let b = reg_email.test(user.email);
        if (!b) loggerEmailTask.warn("不符合邮件发送格式", account_id, user.toString());
        return b;
    });
    if (!users.length) return loggerEmailTask.warn("account 关联 user 没有符合邮件格式的", account_id);

    // 选出需要推送的条目
    let sql = `
        select * from orders_process p right join (
            select * from orders o right join (
                select id as b_basket_id, name as b_basket_name, filename from baskets b where b.account_id = ? and b.status = ?
            ) b on o.basket_id = b.b_basket_id
        ) v on v.id = p.order_id
    `;
    let options = [ account_id, status ];
    let ret = await QUERY(sql, options);
    if (!ret || !ret.length) return loggerEmailTask.warn("不满足邮件发送条件", account_id, status);
    
    // 数据规整
    let baskets = {};
    for (let v of ret){
        if (v.b_basket_name in baskets){
            baskets[v.b_basket_name].push(v);
        } else {
            baskets[v.b_basket_name] = [ v ];
            baskets[v.b_basket_name]["name"] = v.filename;
        }
    }

    // 整理需要发送的邮件格式
    let { display_name, mailbox } = users[0];
    let emails = formatEmailAddress(users, mailbox);
    send_email(baskets, display_name, emails, account_id);
};


const tasks = async () => {

    let email_tasks = await EmailTasks.all({ disabled: 0 }, "*");

    // 将有修改的, 并且停止运行的, 解除定时任务
    let account_ids = email_tasks.map(v => v.account_id);
    for (let key of timers.keys()){
        if (!account_ids.includes(key)){
            timers.get(key)["untimer"]();
            loggerEmailTask.warn("解除邮件定时发送任务", "account_id", key, `共 ${timers.size} 个`);
        }
    }

    if (email_tasks.length){
        for (let item of email_tasks){
            let { account_id, step, status } = item;
            let md5_ci = createMd5(step + status);

            // 1m 无法变更, 只能等下次建立最新的任务, 理论上进度 > 5m
            if (timers.has(account_id)){
                let task = timers.get(account_id);
                // 判断任务的条件是否发生了变化
                if (md5_ci === task["ci"]) continue;
                
                // 解除定时器
                task['untimer']();
                loggerEmailTask.warn("任务发生了变更", account_id, step, status);
            }

            // 新建任务
            let tmp = { ci: md5_ci };
            tmp["timer"] = setTimeout(() => {
                timers.get(account_id)['untimer']();
                get_orders(item);
            }, step * 60 * 1000);

            tmp["untimer"] = function (){
                let task = timers.get(account_id);
                clearTimeout(task["timer"]);
                task["timer"] = null;
                timers.delete(account_id);
            };

            timers.set(account_id, tmp);
            loggerEmailTask.info("邮件定时任务建立", "account_id", account_id, `共 ${timers.size} 个`);
        }

    }

};


module.exports = () => {

    setInterval(tasks, times);
    tasks();

    process.nextTick(() => {
        loggerEmailTask.info("启动 email - tasks");
    });

};

