/**
 * @description
 *  爬取 ccxt 支持的交易所及其方法名
 * 
 */

const fs = require("fs");
const path = require("path");
const cheerio = require('cheerio');

{
 
    const str = fs.readFileSync(path.resolve(__dirname, "../../local/ccxt.txt"));        
    const $ = cheerio.load(str);
    const tmp = {};

    // 获取数据
    const tbodys = $(".markdown-body.entry-content tbody");

    for (let i=0; i<tbodys.length; i++){
        let tbody = tbodys[i];
        let trs = $(tbody).find("tr");

        for (let j=0; j<trs.length; j++){
            let tr = trs[j];
            let tds = $(tr).find("td");

            let k = $(tds[1]).text();
            let v = $(tds[2]).text();

            tmp[v] = k;

        }

    }

    if (JSON.stringify(tmp) !== "{}"){
        fs.writeFileSync(path.resolve(__dirname, "./exchanges.json"), JSON.stringify(tmp, null, "\t"));
    }

}


