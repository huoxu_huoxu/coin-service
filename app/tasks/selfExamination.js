/**
 * @description
 *  启动自检
 * 
 * @function
 *  exam_dir_writable               检查目录是否可写
 * 
 * 
 */

const fs = require("fs");
const {
    logConfigure: { loggerErr, runtime }
} = require("../libs");
const {
    QUERY
} = require("../services/mysqlConnect");
const redis = require("../services/redisConnect");

const { UPLOAD_PATH, LOGGER_PATH } = process.env;


const exam_dir_writable = (dir_path) => {
    runtime.debug(`自检 - 目录可写权限: ${dir_path}`);
    return new Promise((resolve, reject) => {
        fs.access(dir_path, fs.constants.W_OK, (err) => {
            if (err) reject(`${dir_path} 不可写`);
            resolve();
        });
    });
};


const exam_sql_available = async () => {
    runtime.debug("自检 - sql是否可连接");
    await QUERY("select 1");
   
};


const exam_redis_available = async () => {
    runtime.debug("自检 - redis是否可连接");
    await redis.get("a");
};

module.exports = async () => {

    try {

        await exam_dir_writable(LOGGER_PATH);
        await exam_dir_writable(UPLOAD_PATH);

        await exam_sql_available();

        await exam_redis_available();

        runtime.debug("自检完成, 一切正常 .");

    } catch (err){

        loggerErr.fatal(err.toString());
        loggerErr.fatal("存在问题, 退出服务进程 ...");
        process.exit(1);

    }

};

