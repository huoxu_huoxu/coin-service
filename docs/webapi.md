#### 对web方 - api

##### 返回消息格式
	
	{
		errcode: 	返回码, int, 只有为0时才成功, .eg 0,
		msg:		关于这次请求的描述, string, .eg "succ",
		[data]:		返回的数据, object, 只有需要返回数据的接口才有这个字段
	}

##### 新增 account

| URL | /api/user/addAccount |
|-----|----------------------|
|方法	| POST
|描述  | 新增account_id

输入
	
	{
		display_name: "xx",
		name: "xxx",
		user_id: 1,
		exchange: "交易所",
		token: "令牌"
	}
	
输出

	成功
	{
		errcode: 0,
		msg: "succ"
	}
	
	失败
	{
		errcode: 12002,
		msg: "unique 字段不能重复"
	}
	

##### 新增用户

| URL | /api/user/register |
| ----| ------------------- |
| 方法 | POST
| 描述 | 注册平台用户

输入

	{
		email: "530607199@qq.com",
		password: "xxxx",
		nickname:	"昵称",
		cipher: "qscfv"  // 这个是暂时的注册密文字段, 必填
	}
	
 输出
 	
 	成功
 	{
 		errcode: 0,
 		msg: "succ"
 	}
 	
 	失败
 	{
 		errcode: 1000,
 		msg: "email已注册过, 请勿重复注册"
 	}
 	{
 		errcode: 1012,
 		msg: "非法注册"
 	}

	

##### 上传csv篮子文件

| URL | /api/baskets/upload |
|-----|---------------------|
| 方法 | POST
| 描述 | 存放交易集合的篮子文件

输入
	
	{
		backetsCsv: csv文件,
		account_id: 1
	}

输出

	{
		errcode: 0,
		msg: "succ"	
	}


#### 登录账户

| URL | /api/user/login |
|-----|-----------------|
| 方法 | POST 
| 描述 | 用户登录

输入

	{
		email: "",
		password: "",
		verifyValue: "",
		verifyToken: ""
	}
	
输出

	成功
	{
		errcode: 0,
		msg: "succ",
		data: {
			nickname: "xxx",
			email: "xxx",
			token: "xxx"
		}	
	}
	失败
	{
		errcode: 1004,
		msg: "验证图片过期, 请刷新页面或刷新图片"
	}
	{
		errcode: 1006,
		msg: "验证码错误"
	}
	{
		errcode: 1008,
		msg: "账号 / 邮箱 不存在"
	}
	{
		errcode: 1010,
		msg: "账号或密码错误"
	}
	
	
##### 	测试: 获取验证结果

| URL | /api/verify/testGetVG |
|-----|-----------------------
|方法 | GET
|描述 | debug环境 POSTMAN 测试使用

输入 
	
	无	

输出

	{
		v: "token码",
		t: "验证码的结果"
	}


##### 验证用户token

| URL | /api/verify/token?token=xx |
|-----|-----
|方法  | GET
|描述 | 用户登录过后关闭页面，再次访问页面时用于自动登录


输入

	?token=令牌
	
输出
	
	成功
	{
		errcode: 0,
		msg: "succ"
	}
	失败
	{
		errcode: 1100,
		msg: "登录过期"
	}


##### 获取 user id 下的 account 列表
| URL | /api/data/getAccounts?token=xx |
|-----|-----
|方法  | GET
| 描述 | 用户登录后获取管理账户列表

输入

	?token=令牌
	
输出

	{
		errcode: 0,
		msg: "succ",
		data: {
			list: {
				id: 1,
				display_name: "bbb",
				...
			}
		}
	}
	
	
##### 获取代理账户下的篮子列表
| URL | /api/data/getBaskets/:account_id?token=xxx&
|-----|------
|方法  | GET
|描述  | 搜索符合条件的代理账户下的篮子列表, 默认显示全部

输入

	?token=领牌
	status=运行状态
	started_time=开始时间
	ended_time=结束时间
	coin_name=包含哪些货币

输出

	{
		errcode: 0,
		msg: "succ",
		data: {
			id: 1,
			name: "BA..."
			...
		}
	}


##### web端获取订单列表
| URL | /api/data/getOrders/:account_id/:basket_id?token=xxx
| 方法 | GET
| 描述 | 搜索符合条件的某代理账户下某篮子的订单

输入
	
	?token=令牌
	status=运行状态
	coin_name=包含哪些货币
	
输出

	{
		errcode: 0,
		msg: "succ",
		data: {
			list: [
				{
					...
				}
			]
		}
	}
	
	
##### web端允许运行篮子

| URL | /api/readyBasket/:account_id/:basket_id
| ---- | ---
| 方法 | put
| 描述 | 将篮子的运行状态从等待切换到就绪 wait -> new
	
输入

	{
		token: "令牌"
	}

输出
	
	成功
	{
		errcode: 0,
		msg: "succ",
		data: { basket_id: xx }
	}
	失败
	{
		errcode: 4202,
		msg: "无法修改状态非wait的篮子"
	}


##### 重命名篮子

| URL | /api/reanemBasket/:account_id/:basket_id
| ---| ------
| 方法 | PUT
| 描述 | 篮子改名

输入

	{
		token: "令牌",
		new_filename: "新名称"
	}
	
输出

	{
		errcode: 0,
		msg: "succ",
		data: { basket_id: xx, filename: "新名称" }
	}

