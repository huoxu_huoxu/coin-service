#### 对py方 - api

##### 返回消息格式
	
	{
		errcode: 	返回码, int, 只有为0时才成功, .eg 0,
		msg:		关于这次请求的描述, string, .eg "succ",
		[data]:		返回的数据, object, 只有需要返回数据的接口才有这个字段
	}



##### 根据account_name获取新的篮子

| url |  /api/baskets/new/:account_name |
|-----|-------------------------------|
|方法  | GET								|
|描述	| 根据 `代理账户的name` 获取新添加`篮子`的id数组|


输入
	
	无
	
输出

	{
		errcode: 0,
		msg: "succ",
		data: {
			baskets: [
				{
					name: "BA2018...",
					account_name: "aaa"
				},
				{
					name: "BA2018...",
					account_name: "aaa"
				}
			]
		}
	}
		

##### 根据account_name及basket_name获取orders

| URL | /api/baskets/info/:account_name/:basket_name |
|-----|------------------------------------------|
|方法 	| GET
|描述	| 第一次通过 `代理账户name` 和 `篮子name` 获取篮子下所有交易单信息

输入

	无
	
输出

	成功返回:
	{
		errcode: 0,
		msg: "succ",
		data: {
			orders: [
				{
					id: 1,
					basket_id: 1,
					base_asset: "xxx",
					quote_asset: "xxx",
					direction: "xxx",
					algo_name: "EPOV",
					algo_param: "",
					user_algo_param: {
						user_pov: 0,
						user_spread_threshold: 0
					}, // 如果用户没有设置 则为 null
					start_time: null,
					end_time: null,
					qty: 0,
					do_opt: "",
					name: "677dfdad-c451-4728-abd4-8bd234c76bb1"
				},
				...
			]
		}
	}
	
	account_id下没有basket_id:
	{
		errcode: 4000,
		msg: "account_name: xx 下不存在 basket_name: xx"
	}

	
	
##### 获取某个篮子下所有的交易单
	
| URL | /api/baskets/orders/:basket_name |
|-----|--------------------------------|
|方法  | GET
|描述  | 根据 `basket_name` 获取关联的所有交易单的信息|

输入

	无
	
输出

	{
		errcode: 0,
		msg: "succ",
		data: {
			orders: {},
			basket: {}
		}
	}
	
	
	
##### 更新交易单的状态

| URL | /api/orders/updateProcess/:basket_name |
|-----|---------------------------|
| 方法	| PUT
| 描述 | 通过 `交易单order的name` 来更新交易单的进行状态, 通过`backet_name`确保所有的order在一个篮子里

输入

	{
		data: [
			{
				name: "bd767859-4c03-4147-a203-c9eb574f3275",
				trade: 10.09,
				current_algo_param: {
					pov: 0.5,
					spread_threshold: 0.7
				},
				status: "",
				error_msg: ""
			},
			...
		]
	}

输出
	
	全部成功
	{
		errcode: 0,
		msg: "succ"
	}
	
	部分成功
	{
		errcode: 4002,
		msg: "部分order name不存在",
		data: {
			err_name: [
				"bd767859-4c03-4147-a203-c9eb574f3275"
			]
		}
	}
	

##### 更新账户余额信息

| URL | /api/setAccountBalance/:account_name |
| ----| ----
| 方法 | PUT
| 描述 | 更新用户下某账户的货币余额信息

输入
	
	{
		list: [
			{
				asset: "币种",
				balance: "数量"
			},
			...
		]
	}
	
输出

	成功
	{
		errcode: 0,
		msg: "succ"
	}
	失败
	{
		errcode: 4020,
		msg: "数据格式不正确"
	}
	
	
##### 更新篮子信息(目前提供状态更新)

| URL | /api/updateBasket/:basket_name |
|-----|---------
| 方法 | PUT
| 描述 | 更新篮子

输入
	
	{
		status: "finished"	
	}
	
输出
	
	成功
	{
		errcode: 0,
		msg: "succ"
	}
	失败
	{
		errcode: 4008,
		msg: "无效basket name"
	}

